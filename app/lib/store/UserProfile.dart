class UserProfile {
    Map<String, dynamic> user = {
        "name": "",
        "lastName": "",
        "email": "",
        "picture": "",
        "state": "",
        "birthDate": ""
    };

    setUserData(Map<String, dynamic> data){
        for(String key in data.keys){
            this.user[key] = data[key];
        }
    }
}