import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationsProvider {

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  initNotifications(){

    _firebaseMessaging.requestNotificationPermissions();

    _firebaseMessaging.getToken().then(( token ){

      print('===== FCM  Token =====');
      print(token);

    });

    _firebaseMessaging.configure(

      onMessage: ( info ) async{

        print('===== On Message =====');
        print( info );
      },
      onLaunch: ( info ) async{

        print('===== On Launch =====');
        print( info );
      },
      onResume: ( info ) async{

        print('===== On Resume =====');
        print( info );
      }
    );

  }



}