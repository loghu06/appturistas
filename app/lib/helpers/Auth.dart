import 'package:Turistas/graphql/Types.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class AuthHelper {
    // Sign in using email and password
    static QueryOptions signInOptions(LoginData loginData){
        return QueryOptions(
            document: r'''
                query Login($email: String!, $password: String!){
                    Login(email: $email, password: $password){
                        message isSuccess
                    }
                }
            ''',
            variables: <String, String>{
                "email": loginData.email,
                "password": loginData.password
            }, // Always has to be network only
            fetchPolicy: FetchPolicy.networkOnly
        );
    }

    // Sign up using email and password
    static MutationOptions signUpOptions(SignUpData signUpData){
        return MutationOptions(
            document: r'''
                mutation User($user: UserInput!){
                    User(user: $user){
                        message isSuccess token
                    }
                }
            ''',
            variables: <String, dynamic>{
                "user": signUpData.getData() // Gat all data stored
            },
            fetchPolicy: FetchPolicy.networkOnly
        );
    }

    // Sign in using a Facebook
    static QueryOptions facebookSignIn(String token){
        return QueryOptions(
            document: r'''
                query FacebookSignIn($token: String!){
                    FacebookSignIn(token: $token){
                        message isSuccess
                    }
                }
            ''',
            variables: <String, String>{
                "token": token
            },
            fetchPolicy: FetchPolicy.networkOnly
        );
    }

    // Sign up using a Facebook
    static MutationOptions facebookSignUp(String token){
        return MutationOptions(
            document: r'''
                mutation FacebookSignUp($token: String!){
                    FacebookSignUp(token: $token){
                        message isSuccess
                    }
                }
            ''',
            variables: <String, String>{
                "token": token
            },
            fetchPolicy: FetchPolicy.networkOnly
        );
    }
}