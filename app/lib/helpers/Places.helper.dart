import 'package:graphql_flutter/graphql_flutter.dart';

class PlacesHelper {
  static QueryOptions getPlaces(){
    return QueryOptions(
      document: r'''
        query {
          Places {
            _id name latitude longitude kindOfPlace { kind }
          }
        }
      ''',
      fetchPolicy: FetchPolicy.cacheAndNetwork
    );
  }

  static QueryOptions getReview(String place){
    return QueryOptions(
      document: r'''
        query Review($place: String!){
          Review(place: $place){
            _id rating message
          }
        }
      ''',
      variables: {
        "place": place
      },
      fetchPolicy: FetchPolicy.networkOnly
    );
  }

  static QueryOptions getReviews(String place){
    return QueryOptions(
      document: r'''
        query PlaceReviews($place: String!){
          PlaceReviews(place: $place){
            _id rating message user { name lastName }
          }
        }
      ''',
      variables: {
        "place": place
      },
      fetchPolicy: FetchPolicy.networkOnly
    );
  }

  static MutationOptions makeReview(Map<String, dynamic> reviewData){
    return MutationOptions(
      document: r'''
        mutation Review($review: ReviewInput!){
          Review(review: $review){
            message isSuccess
          }
        }
      ''',
      variables: {
        "review": reviewData
      }
    );
  }

  static MutationOptions updateReview(Map<String, dynamic> reviewData){
    return MutationOptions(
      document: r'''
        mutation UpdateReview($review: ReviewInput!){
          UpdateReview(review: $review){
            message isSuccess
          }
        }
      ''',
      variables: {
        "review": reviewData
      }
    );
  }
}