import 'package:graphql_flutter/graphql_flutter.dart';

class UserHelper {

    static QueryOptions getUser({FetchPolicy fetchPolicy = FetchPolicy.networkOnly}){
        return QueryOptions(
            document: r'''
                query {
                    User {
                        name lastName email picture state birthDate
                    }
                }
            ''',
            fetchPolicy: fetchPolicy
        );
    }
    
    static MutationOptions userUpdate(Map<dynamic, dynamic> user){
        return MutationOptions(
            document: r'''
                mutation UpdateUser($user: UserInput!){
                    UpdateUser(user: $user){
                        message isSuccess
                    }
                }
            ''',
            variables: <String, dynamic>{
                "user": user
            }
        );
    }
}