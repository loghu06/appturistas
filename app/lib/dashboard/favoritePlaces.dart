import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FavoritePlacesPage extends StatefulWidget {
    @override
    FavoritePlaceState createState() => FavoritePlaceState();
}

class FavoritePlaceState extends State<FavoritePlacesPage> {
    final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
    Map<String, bool> _selectedPlaces = <String, bool>{
        "park": false,
        "museum": false,
        "hotel": false,
        "bar": false,
        "shopping": false,
        "food": false
    };
    MediaQueryData _deviceDetails;
    Scaffold _rootWidget;
    MaterialButton _saveButton;
    Column _placesColumn;
    ListView _parksList;
    ListView _museumsList;
    ListView _hotelsList;
    ListView _barsList;
    ListView _shoppingList;
    ListView _foodList;
    
    @override
    Widget build(BuildContext context) {
        this._deviceDetails = MediaQuery.of(context);
        this._initWidgets();
        
        return this._rootWidget;
    }

    DecorationImage _getImageDecoration({String url}){
        return DecorationImage(
            // image: NetworkImage(url),
            image: AssetImage(url),
            fit: BoxFit.cover
        );
    }

    Container _getListContainer({String picture}){
        return Container(
            width: 350,
            height: 100,
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: this._getImageDecoration(url: picture)
            ),
        );
    }

    Container _getPlacesList({
        ListView list,
        String listTitle,
        String placeName
    }){
        return Container(
            height: 300,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                    Container(
                        height: 250,
                        child: list,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 20
                        ),
                        child: Row(
                            children: <Widget>[
                                Text(
                                    listTitle,
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold
                                    ),
                                ),
                                IconButton(
                                    icon: Icon(
                                        this._selectedPlaces[placeName] ? Icons.favorite : Icons.favorite_border,
                                        color: Colors.blue,
                                    ),
                                    onPressed: (){
                                        setState(() {
                                            this._selectedPlaces[placeName] = !this._selectedPlaces[placeName];                                          
                                        });
                                    },
                                )
                            ],
                        )
                    ),
                ],
            ),
        );
    }

    void _initWidgets(){
        this._parksList = ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
                this._getListContainer(picture: "assets/images/parks/park1.jpg"),
                this._getListContainer(picture: "assets/images/parks/park2.jpg"),
                this._getListContainer(picture: "assets/images/parks/park3.jpg"),
                this._getListContainer(picture: "assets/images/parks/park4.jpg"),
                this._getListContainer(picture: "assets/images/parks/park5.jpg")
            ],
        );
        
        this._museumsList = ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
                this._getListContainer(picture: "assets/images/museums/museum1.jpg"),
                this._getListContainer(picture: "assets/images/museums/museum2.jpg"),
                this._getListContainer(picture: "assets/images/museums/museum3.jpg"),
                this._getListContainer(picture: "assets/images/museums/museum4.jpg"),
                this._getListContainer(picture: "assets/images/museums/museum5.jpg"),
            ],
        );
        
        this._foodList = ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
                this._getListContainer(picture: "assets/images/food/food1.jpg"),
                this._getListContainer(picture: "assets/images/food/food2.jpg"),
                this._getListContainer(picture: "assets/images/food/food3.jpg"),
                this._getListContainer(picture: "assets/images/food/food4.jpg"),
                this._getListContainer(picture: "assets/images/food/food5.jpg"),
            ],
        );

        this._barsList = ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
                this._getListContainer(picture: "assets/images/bars/bar1.jpg"),
                this._getListContainer(picture: "assets/images/bars/bar2.jpg"),
                this._getListContainer(picture: "assets/images/bars/bar3.jpg"),
                this._getListContainer(picture: "assets/images/bars/bar4.jpg")
            ],
        );

        this._hotelsList = ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
                this._getListContainer(picture: "assets/images/hotels/hotel1.jpg"),
                this._getListContainer(picture: "assets/images/hotels/hotel2.jpg"),
                this._getListContainer(picture: "assets/images/hotels/hotel3.jpg"),
                this._getListContainer(picture: "assets/images/hotels/hotel4.jpg"),
                this._getListContainer(picture: "assets/images/hotels/hotel5.jpg"),
            ],
        );
        
        this._shoppingList = ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
                this._getListContainer(picture: "assets/images/shopping/shopping1.jpg"),
                this._getListContainer(picture: "assets/images/shopping/shopping2.jpg"),
                this._getListContainer(picture: "assets/images/shopping/shopping3.jpg"),
                this._getListContainer(picture: "assets/images/shopping/shopping4.jpg")
            ],
        );
        
        this._placesColumn = Column(
            children: <Widget>[
                this._getPlacesList(
                    listTitle: "Parques",
                    list: this._parksList,
                    placeName: "park"
                ),
                this._getPlacesList(
                    listTitle: "Museos",
                    list: this._museumsList,
                    placeName: "museum"
                ),
                this._getPlacesList(
                    listTitle: "Comida",
                    list: this._foodList,
                    placeName: "food"
                ),
                this._getPlacesList(
                    listTitle: "Bares",
                    list: this._barsList,
                    placeName: "bar"
                ),
                this._getPlacesList(
                    listTitle: "Hoteles",
                    list: this._hotelsList,
                    placeName: "hotel"
                ),
                this._getPlacesList(
                    listTitle: "Centros comerciales",
                    list: this._shoppingList,
                    placeName: "shopping"
                ),
            ],
        );
        
        this._saveButton = MaterialButton(
            child: Text(
                "GUARDAR",
                style: TextStyle(
                    color: Colors.white
                ),
            ),
            minWidth: this._deviceDetails.size.width,
            height: 60,
            color: Colors.blue[500],
            onPressed: (){},
        );
        
        this._rootWidget = Scaffold(
            appBar: AppBar(
                title: Text("Lugares favoritos"),
            ),
            body: Stack(
                children: <Widget>[
                    Align(
                        child: Container(
                            margin: EdgeInsets.only(
                                bottom: 60,
                            ),
                            child: SingleChildScrollView(
                                child: this._placesColumn,
                            ),
                        ),
                    ),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: this._saveButton
                    )
                ],
            ),
        );
    }
}