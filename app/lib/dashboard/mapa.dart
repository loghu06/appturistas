import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dashboard.dart';

class Mapa extends StatefulWidget {
    @override
    _MapaState createState() => _MapaState();
}

class _MapaState extends State<Mapa> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        builder: (_) => DirectionProvider(),
        child: Dashboard()
    );
  }
}