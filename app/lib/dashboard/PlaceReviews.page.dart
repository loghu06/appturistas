import 'package:Turistas/helpers/Places.helper.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class PlaceReviewsPage extends StatefulWidget {
    PlaceReviewsPageState createState() => PlaceReviewsPageState();
}

class PlaceReviewsPageState extends State<PlaceReviewsPage>{
  GraphQLClient _client;
  String _placeID;
  MediaQueryData _deviceDetails;
  Map<String, dynamic> _placeData = {
    "_id": ""
  };
  // Widgets
  Scaffold _rootWidget;
  MaterialButton _writeReviewButton;
  Query _reviewsQuery;
  Container _noReviewsText;
  // Column _reviewRow;

  @override
  Widget build(BuildContext context) {
    this._placeData = ModalRoute.of(context).settings.arguments;
    print(this._placeData);
    this._deviceDetails = MediaQuery.of(context);
    // print(this._placeData["_id"]);
    this._client = GraphQLProvider.of(context).value;
    this._initWidgets();
    
    return this._rootWidget;
  }

  Column _reviewRow({
    Map<String, dynamic> review
  }){
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            bottom: 10
          ),
          child: Text(
            review["user"]["name"] + " " + review["user"]["lastName"],
            style: TextStyle(
              color: Colors.black87,
              fontSize: 25
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            bottom: 10
          ),
          child: Text(
            "Calificación: " + review["rating"].toString() + " / 5",
            style: TextStyle(
              color: Colors.black45
            ),
          ),
        ),
        Text(
          review["message"],
          style: TextStyle(
            color: Colors.black45,
            fontSize: 22
          ),
        ),
        Divider()
      ],
    );
  }

  void _initWidgets(){
    this._noReviewsText = Container(
      padding: EdgeInsets.all(20),
      child: Text(
        "Aún no hay comentarios sobre este lugar",
        style: TextStyle(
          color: Colors.grey[700],
          fontSize: 24
        ),
        textAlign: TextAlign.center,
      )
    );

    this._writeReviewButton = MaterialButton(
      child: Text(
        "ESCRIBIR UN COMENTARIO",
        style: TextStyle(
          color: Colors.white
        ),
      ),
      color: Colors.blue,
      height: 50,
      minWidth: this._deviceDetails.size.width,
      onPressed: (){
        Navigator.of(context).pushNamed("writeReview", arguments: this._placeData);
      },
    );

    this._reviewsQuery = Query(
      options: PlacesHelper.getReviews(this._placeData["_id"]),
      builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }){
        // print("Resultados " + result.data.toString());
        // print(result.data["PlaceReviews"]);
        // print(result.data);
        List reviews = [];
        
        if(result.data != null){
          reviews = result.data["PlaceReviews"];
          
          if(reviews.length > 0){
            return ListView.builder(
              itemCount: reviews.length,
              itemBuilder: (context, index){
                final review = reviews[index];
                
                return Container(
                  margin: EdgeInsets.only(
                    top: 20,
                    right: 10,
                    left: 10
                  ),
                  child: this._reviewRow(review: review)
                );
              },
            );
          }
          else {
            return this._noReviewsText;
          }
        }
        else {
          return this._noReviewsText;
        }
      },
    );

    this._rootWidget = Scaffold(
      appBar: AppBar(
        title: Text("Comentarios"),
      ),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: this._reviewsQuery,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: this._writeReviewButton,
          ),
        ],
      ),
    );
  }
}