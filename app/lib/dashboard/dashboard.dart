import 'dart:async';
import 'package:Turistas/helpers/User.dart';
import 'package:Turistas/store/UserProfile.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:Turistas/dashboard/profile.page.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'dart:math';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as maps;
import 'package:google_maps_webservice/directions.dart' as ho;
import 'package:permission_handler/permission_handler.dart';
import 'package:location/location.dart' as loc;
import 'package:provider/provider.dart';
import 'package:Turistas/helpers/Places.helper.dart';

class Dashboard extends StatefulWidget {
  @override
  DashboardState createState() => DashboardState();
}

class DashboardState extends State<Dashboard> {
  int _currentIndex = 0;
  // Development only, another widgets for map and user's profile will be here
  final _pages = <Widget>[
    /* Center(child: Text("Notificaciones"),), */
    Center(child: MapPage(),),
    ProfilePage()
  ];
  GraphQLClient _client; // GraphQL client
  Map<String, dynamic> _userProfile;
  List<dynamic> _places;

  @override
  Widget build(BuildContext context){
    WidgetsBinding.instance.addPostFrameCallback((_){
      this._client = GraphQLProvider.of(context).value; // Init GraphQLClient
    });
    
    // Get all places
    // this._getPlaces();
    
    return Injector(
      inject: [
        Inject<UserProfile>(() => UserProfile())
      ],
      builder: (context, _){
        final ModelStatesRebuilder _userProfileStore = Injector.getAsModel<UserProfile>();
        this._getUserProfile(_userProfileStore);
        
        return Scaffold(
          body: this._rootWidget(),
          bottomNavigationBar: this._bottomNavigation(),
        );
      },
    );
  }

  void _getUserProfile(ModelStatesRebuilder _userStore) async {
    QueryResult data = await this._client.query(UserHelper.getUser(fetchPolicy: FetchPolicy.cacheAndNetwork));
    // this._userProfile = data.data["User"];
    _userStore.setState((state) => state.setUserData(data.data["User"]));
  }

  Widget _rootWidget(){
    return this._pages[this._currentIndex];
  }

  List _navigationItems(){
    return <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          Icons.map
        ),
        title: Text("Lugares"),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.person), 
        title: Text("Perfil"),
      )
    ];
  }

  Widget _bottomNavigation(){
    return BottomNavigationBar(
      items: this._navigationItems(),
      currentIndex: this._currentIndex,
      type: BottomNavigationBarType.fixed,
      fixedColor: Colors.blue,
      onTap: (int index){
        setState(() { 
          _currentIndex = index;
        });
      },
    );
  }
}

class MapPage extends StatefulWidget {
  //22.7608698,-102.5765658
  final LatLng fromPoint = LatLng(22.7611157,-102.5791257); //posicion de inicio para la ruta
  final LatLng toPoint = LatLng(22.745897, -102.5166383); //posicion de fin para la ruta
  //  final LatLng _ho = ; //museo guadalupe
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  GraphQLClient _client;
  List<dynamic> _places = [];
  Set<Marker> _markers;
  Map<String, dynamic> _userProfile = {
    "name": "",
    "lastName": "",
    "email": "",
    "picture": "",
    "state": "",
    "birthDate": ""
  };

  var location = new loc.Location();
  void posicion() {
    location.onLocationChanged().listen((loc.LocationData currentLocation) {
      //print(currentLocation.latitude);
      //print(currentLocation.longitude);
      double lat;
      lat = currentLocation.latitude;
      double lon;
      lon = currentLocation.longitude;
     print('posicion actual: ' + '$lat' + ',' + '$lon');//posicion actual
    });
  }

  MapType _defaultMapType = MapType.normal;

  void _changeMapType() {
    setState(() {
      _defaultMapType = _defaultMapType == MapType.normal
        ? MapType.satellite
        : MapType.normal;
    });
  }

  void _getUserProfile() async {
    QueryResult data = await this._client.query(UserHelper.getUser(fetchPolicy: FetchPolicy.cacheAndNetwork));
    this._userProfile = data.data["User"];
    // _userStore.setState((state) => state.setUserData(data.data["User"]));
  }

  PermissionStatus _status;

  GoogleMapController _mapController;
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => {
      this._client = GraphQLProvider.of(context).value
    });
    this._getUserProfile();

    // Get all places
    this._getPlaces();
    
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          'Zacatecas',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.amber[300],
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add_alert),
            color: Colors.black87,
            onPressed: _askpermission,
          ),
        ],
      ),
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountEmail: Text(this._userProfile["email"]),
              accountName: Text(this._userProfile["name"] + " " + this._userProfile["lastName"]),
              currentAccountPicture: CircleAvatar(
                child: Text(this._userProfile["name"][0]),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 25.0),
            ),
            ListTile(
                leading: Icon(FontAwesomeIcons.monument,
                    color: Colors.blue, size: 30.0),
                title: Text(
                  'Museos',
                  style: TextStyle(fontSize: 20.0),
                ),
                onTap: (){
                  print("Museos");
                },
                /* onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Dashboard())) */
            ),
            ListTile(
              leading: Icon(
                FontAwesomeIcons.beer,
                color: Colors.blue,
                size: 30.0,
              ),
              title: Text('Bares', style: TextStyle(fontSize: 20.0)),
            ),
            ListTile(
              leading:
                  Icon(FontAwesomeIcons.coffee, color: Colors.blue, size: 30),
              title: Text('Cafeterias', style: TextStyle(fontSize: 20.0)),
            ),
            ListTile(
              leading: Icon(Icons.local_florist, color: Colors.blue, size: 30),
              title: Text('Parques', style: TextStyle(fontSize: 20.0)),
            ),
          ],
        ),
        //Text('hola'),
      ),
      body: Consumer<DirectionProvider>(
        builder: (BuildContext context, DirectionProvider api, Widget child) {
          return GoogleMap(
            initialCameraPosition: CameraPosition(
              target: widget.fromPoint,
              zoom: 12,
            ),
            mapType: _defaultMapType,
            markers: this._markers,
          // markers: Set.from(allMarkers),
            polylines: api.currentRoute,
            onMapCreated: _onMapCreated,
            myLocationEnabled: true,
            myLocationButtonEnabled: true
          );
        }
      ),
      /* floatingActionButton: FloatingActionButton(
          child: Icon(Icons.ac_unit),
          onPressed: () {
            _centerView();
            posicion();
            print('ruta en el centro');
          }
          // _changeMapType,
          ), */
    );
  }

  // Obtiene todos los lugares de la base de datos
  Future<void> _getPlaces() async {
    QueryResult _response = await this._client.query(PlacesHelper.getPlaces());
    setState(() {
      this._places = _response.data["Places"];
      this._createMarkers();
    });
    // print(this._places);
  }

  void _updateStatus(PermissionStatus status) {
    if (status != _status) {
      setState(() {
        _status = status;
      });
    }
  }
  
  void _askpermission() {
    PermissionHandler().requestPermissions(
      [PermissionGroup.locationWhenInUse]).then(_onStatusRequested);
  }

  void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
    final status = statuses[PermissionGroup.locationWhenInUse];
    if (status == PermissionStatus.granted) {
      PermissionHandler().openAppSettings();
    } else if (status == PermissionStatus.denied) {
      Navigator.of(context).push(
        MaterialPageRoute(builder: (BuildContext context) => Dashboard()));
    } else {
      print('hola3');
      PermissionHandler().openAppSettings();
    }
  }

  // Obtiene un tipo de marcador segun el tipo de lugar
  Future<BitmapDescriptor> _getMarkerOf({String placeType}) async {
    BitmapDescriptor _bitmap;
    
    switch(placeType){
      case "Coffee": {
        // _bitmap = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueYellow);
        _bitmap = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(),
          "assets/images/markers/coffee-icon.png"
        );
      } break;
      case "Bar": {
        _bitmap = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(),
          "assets/images/markers/beer-icon.png"
        );
      } break;
      case "Park": {
        _bitmap = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(),
          "assets/images/markers/park-icon.png"
        );
      } break;
      case "Museum": {
        _bitmap = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(),
          "assets/images/markers/museum-icon.png"
        );
      } break;
      default: {
        _bitmap = BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed);
      } break;
    }

    return _bitmap;
  }

  Future<Set<Marker>> _createMarkers() async {
    var tmp = Set<Marker>();
    
    for(int i = 0 ; i < this._places.length ; i++){
      tmp.add(
        Marker(
          markerId: MarkerId(this._places[i]["_id"]),
          position: LatLng(this._places[i]["latitude"], this._places[i]["longitude"]),
          infoWindow: InfoWindow(
            title: this._places[i]["name"],
            snippet: "Pulsa para escribir una reseña del lugar",
            onTap: (){
            //   Navigator.pushNamed(context, "writeReview", arguments: this._places[i]);
            Navigator.pushNamed(context, "placeReviews", arguments: this._places[i]);
            }
          ),
          icon: await this._getMarkerOf(placeType: this._places[i]["kindOfPlace"]["kind"]) 
        )
      );
    }

    setState(() {
      this._markers = tmp;
    });
    return tmp;
  }

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
    _centerView();
  }

  _centerView() async {
    var api = Provider.of<DirectionProvider>(context);
    await _mapController.getVisibleRegion();
    print("buscando direcciones");
    await api.findDirections(widget.fromPoint, widget.toPoint);

    var left = min(widget.fromPoint.latitude, widget.toPoint.latitude);
    var right = max(widget.fromPoint.latitude, widget.toPoint.latitude);
    var top = max(widget.fromPoint.longitude, widget.toPoint.longitude);
    var bottom = min(widget.fromPoint.longitude, widget.toPoint.longitude);

    api.currentRoute.first.points.forEach((point) {
      left = min(left, point.latitude);
      right = max(right, point.latitude);
      top = max(top, point.longitude);
      bottom = min(bottom, point.longitude);
    });

    var bounds = LatLngBounds(
      southwest: LatLng(left, bottom),
      northeast: LatLng(right, top),
    );
    var cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
    _mapController.animateCamera(cameraUpdate);
  }
}

class DirectionProvider extends ChangeNotifier {
  ho.GoogleMapsDirections directionsApi =
      ho.GoogleMapsDirections(apiKey: "AIzaSyAbhj66jkFE7jUG9XAT0XjEdM8tRLptH88");

  Set<maps.Polyline> _route = Set();

  Set<maps.Polyline> get currentRoute => _route;

  findDirections(maps.LatLng from, maps.LatLng to) async {
    var origin = ho.Location(from.latitude, from.longitude);
    var destination = ho.Location(to.latitude, to.longitude);

    var result = await directionsApi.directionsWithLocation(
      origin,
      destination,
      travelMode: ho.TravelMode.driving,
    );

    Set<maps.Polyline> newRoute = Set();

    if (result.isOkay) {
      var route = result.routes[0];
      var leg = route.legs[0];

      List<maps.LatLng> points = [];

      leg.steps.forEach((step) {
        points.add(maps.LatLng(step.startLocation.lat, step.startLocation.lng));
        points.add(maps.LatLng(step.endLocation.lat, step.endLocation.lng));
      });

      
      notifyListeners();
    } else {
      print("ERRROR !!! ${result.status}");
    }
  }
}

//apikey AIzaSyAbhj66jkFE7jUG9XAT0XjEdM8tRLptH88