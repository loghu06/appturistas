import 'package:Turistas/helpers/Places.helper.dart';
import 'package:Turistas/helpers/Validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class WriteReviewPage extends StatefulWidget {
  WriteReviewPageState createState() => WriteReviewPageState();
}

class WriteReviewPageState extends State<WriteReviewPage>{
  MediaQueryData _deviceDetails;
  GlobalKey<FormState> _formState = GlobalKey();
  GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
  GraphQLClient _client;
  bool _hasReview = false;
  Map<String, dynamic> _arguments;
  Map<String, dynamic> _reviewData = <String, dynamic>{
    "message": "",
    "rating": 3.0,
    "place": ""
  };
  // Widgets
  Scaffold _rootPage;
  Text _title;
  MaterialButton _saveButton;
  Container _formContainer;
  Form _reviewForm;
  TextFormField _message;
  RatingBar _ratingBar;
  double _rating;
  
  @override
  Widget build(BuildContext context){
    this._arguments = ModalRoute.of(context).settings.arguments;
    this._client = GraphQLProvider.of(context).value;
    this._deviceDetails = MediaQuery.of(context); // Get device details
    this._initWidgets(); // Init widgets
    this._getReview();
    this._reviewData["place"] = _arguments["_id"];

    return this._rootPage;
  }

  void _getReview() async {
    QueryResult response = await this._client.query(PlacesHelper.getReview(this._arguments["_id"]));
    // print(response.data["Review"]["message"]);
    print(response.errors);
    
    if(response.data == null){
      print("No hay review");
    }
    else {
      setState(() {
        // this._reviewData["message"] = response.data["Review"]["message"].toString();
        this._hasReview = true;
        this._reviewData["rating"] = double.parse(response.data["Review"]["rating"].toString());
        this._reviewData["message"] = response.data["Review"]["message"].toString();
        this._reviewData["_id"] = response.data["Review"]["_id"];
      });
    }
  }

  void _saveReview() async {
    if(this._formState.currentState.validate()){
      this._formState.currentState.save();

      if(this._hasReview){
        QueryResult result = await this._client.mutate(PlacesHelper.updateReview(this._reviewData));
        
        if(result.data != null){
          this._scaffoldState.currentState.showSnackBar(
            SnackBar(
                content: Text(result.data["UpdateReview"]["message"]),
                duration: Duration(seconds: 5),
                action: SnackBarAction(
                  label: "CERRAR",
                  onPressed: (){
                      Navigator.pop(context);
                  },
                ),
              )
          );
        }
      }
      else {
        QueryResult result = await this._client.mutate(PlacesHelper.makeReview(this._reviewData));

        if(result.data != null){
            this._scaffoldState.currentState.showSnackBar(
              SnackBar(
                  content: Text(result.data["Review"]["message"]),
                  duration: Duration(seconds: 5),
                  action: SnackBarAction(
                  label: "CERRAR",
                  onPressed: (){
                      Navigator.pop(context);
                  },
                  ),
              )
            );
        }
      }
    }
  }

  InputDecoration _formFieldDecoration({
    String label
  }){
    OutlineInputBorder _borderDecoration = OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey
      )
    );
    
    return InputDecoration(
      labelText: label,
      enabledBorder: _borderDecoration,
      border: _borderDecoration,
      focusedBorder: _borderDecoration
    );
  }

  void _initWidgets(){
    this._title = Text(
      "Cuenta como fue tu experiencia visitando este lugar y ayuda a otros turistas",
      style: TextStyle(
        fontSize: 20,
        color: Colors.grey[700],
      ),
      textAlign: TextAlign.center,
    );
    
    this._ratingBar = RatingBar(
      initialRating: this._reviewData["rating"],
      direction: Axis.horizontal,
      itemCount: 5,
      tapOnlyMode: true,
      itemBuilder: (context, _) => Icon(
        Icons.favorite,
        color: Colors.blue,
      ),
      onRatingUpdate: (rating){
        this._reviewData["rating"] = rating;
      },
    );
    
    this._message = TextFormField(
      maxLines: null,
      keyboardType: TextInputType.multiline,
      decoration: this._formFieldDecoration(label: "Describe tu experiencia"),
      validator: (String message) => ValidatorsHelper.notEmpty(message),
      onSaved: (String message){
        this._reviewData["message"] = message;
      },
      initialValue: this._reviewData["message"],
    );
    
    this._reviewForm = Form(
      key: this._formState,
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              top: 40,
            ),
            child: Text(
              "¿Qué tan buena fue tu expericia en este lugar?"
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 20,
              bottom: 20
            ),
            child: this._ratingBar,
          ),
          Container(
            margin: EdgeInsets.only(
              bottom: 20
            ),
            child: this._message,
          )
        ],
      ),
    );

    this._saveButton = MaterialButton(
      child: Text(
        "GUARDAR",
        style: TextStyle(
          color: Colors.white
        ),
      ),
      color: Colors.blue,
      height: 50,
      minWidth: this._deviceDetails.size.width,
      onPressed: (){
        this._saveReview();
      },
    );
    
    this._formContainer = Container(
      width: this._deviceDetails.size.width - 40,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5
          )
        ]
      ),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            this._title,
            this._reviewForm
          ],
        ),
      ),
    );
    
    this._rootPage = Scaffold(
      key: this._scaffoldState,
      appBar: AppBar(
        title: Text(
          "Escribe una reseña"
        ),
      ),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: this._formContainer,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: this._saveButton,
          )
        ],
      ),
    );
  }
}