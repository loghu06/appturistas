import 'package:Turistas/helpers/StoreHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:Turistas/store/UserProfile.dart';

class ProfilePage extends StatefulWidget {
    ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
    GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();
    MediaQueryData _deviceDetails; // Device details (width, height)
    GraphQLClient _client; // GraphQL client
    var _userStore;
    Map<String, dynamic> _userProfile = {
        "name": "",
        "lastName": ""
    };
    StoreHelper _storeHelper = new StoreHelper();
    Scaffold _pageRoot; // Root widget
    Container _profileBackground; // Gradient background
    Container _profileDetailsContainer;
    Container _profileDetails; // Profile details (name, lastname, email, picture)
    Container _profilePictureContainer;
    Container _profileActions;
    BoxDecoration _buttonDecoration;
    TextStyle _textStyleDecoration;
    MaterialButton _updateProfileButton;
    MaterialButton _updatePlacesButton;
    MaterialButton _signOut;
    Image _profilePicture; // Profile picture
    
    Widget build(BuildContext context){
        // Init store
        this._userStore = Injector.getAsModel<UserProfile>(context: context);
        this._userProfile = this._userStore.state.user;
        this._profilePicture = Image.network(this._userProfile["picture"]);
        // Init GraphQL client
        this._deviceDetails = MediaQuery.of(context);
        WidgetsBinding.instance.addPostFrameCallback((_){
            this._client = GraphQLProvider.of(context).value; // Init GraphQL client
            // this._getUserProfile();
        });
        // Init widgets
        this._initWidgets();
        return this._pageRoot;
    }

    @override
    void initState() {
        super.initState();
    }

    void _initWidgets(){
        this._profileBackground = Container(
            // color: Colors.blue,
            height: this._deviceDetails.size.width / 2,
            width: this._deviceDetails.size.width,
            padding: EdgeInsets.all(30),
            decoration: new BoxDecoration(
                color: Colors.blue,
                gradient: LinearGradient(
                    colors: [Color.fromARGB(255, 228, 71, 120), Colors.blue],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight
                )
            ),
        );

        // Profile picture container
        this._profilePictureContainer = Container(
            height: this._deviceDetails.size.width / 3,
            width:  this._deviceDetails.size.width / 3,
            child: ClipRRect(
                child: this._profilePicture,
                borderRadius: BorderRadius.circular(100),
            ),
            margin: EdgeInsets.only(
                top: (this._deviceDetails.size.width / 4) - (this._deviceDetails.size.width / 6)
            ),
        );

        this._profileDetails = Container(
            margin: EdgeInsets.only(
                top: this._deviceDetails.size.width / 6
            ),
            padding: EdgeInsets.all(10),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                    Text(
                        '${this._userProfile["name"]} ${this._userProfile["lastName"]}',
                        style: TextStyle(
                            fontSize: 22,
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.bold
                        ),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                            Column(
                                children: <Widget>[
                                    Text(
                                        "22",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            fontFamily: "Montserrat"
                                        ),
                                    ),
                                    Text(
                                        "Comentarios",
                                        style: TextStyle(
                                            fontSize: 16
                                        ),
                                    )
                                ],
                            )
                        ],
                    )
                ],
            )
        );

        this._profileDetailsContainer = Container(
            width: this._deviceDetails.size.width,
            height: this._deviceDetails.size.width / 2,
            margin: EdgeInsets.fromLTRB(
                30, (this._deviceDetails.size.width/2) - (this._deviceDetails.size.width/4),
                30, 0),
            child: this._profileDetails,
            decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: new BorderRadius.all(const Radius.circular(25)),
                boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 5
                    )
                ]
            ),
        );

        this._buttonDecoration = BoxDecoration(
            borderRadius: new BorderRadius.all(
                const Radius.circular(10)
            ),
            color: Colors.blueAccent
            /* gradient: LinearGradient(
                colors: [
                    Color.fromARGB(255, 228, 71, 120),
                    Colors.blueAccent
                ],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight
            ) */
        );

        this._textStyleDecoration = TextStyle(
            color: Colors.white,
            fontFamily: "Montserrat",
            fontWeight: FontWeight.w900,
            fontSize: 18
        );

        this._updateProfileButton = MaterialButton(
            child: Text(
                "Actualizar perfil",
                style: this._textStyleDecoration,
            ),
            minWidth: this._deviceDetails.size.width - 40,
            height: 50,
            onPressed: (){
                Navigator.pushNamed(context, "editProfile");
                // Navigator.pushNamed(this._scaffoldState.currentContext, "editProfile");
            },
        );

        this._updatePlacesButton = MaterialButton(
            child: Text(
                "Lugares favoritos",
                style: this._textStyleDecoration,
            ),
            minWidth: this._deviceDetails.size.width - 40,
            height: 50,
            onPressed: (){
                Navigator.of(context).pushNamed("favoritePlaces");
            },
        );
        
        this._signOut = MaterialButton(
            child: Text(
                "Cerrar sesión",
                style: this._textStyleDecoration,
            ),
            minWidth: this._deviceDetails.size.width - 40,
            height: 50,
            onPressed: (){
                this._storeHelper.removeToken();
                Navigator.pushReplacementNamed(context, "signIn");
            },
        );
        
        this._profileActions = Container(
            width: this._deviceDetails.size.width - 40,
            height: this._deviceDetails.size.height,
            margin: EdgeInsets.fromLTRB(
                20, (this._deviceDetails.size.width / 2) + (this._deviceDetails.size.width / 4) + 20,
                20, 10),
            child: Column(
                /* mainAxisAlignment: MainAxisAlignment.spaceEvenly, */
                children: <Widget>[
                    Container(
                        child: this._updateProfileButton,
                        decoration: this._buttonDecoration,
                        margin: EdgeInsets.only(bottom: 10),
                    ),
                    /* Container(
                        child: this._updatePlacesButton,
                        decoration: this._buttonDecoration,
                        margin: EdgeInsets.only(bottom: 10),
                    ), */
                    Container(
                        child: this._signOut,
                        decoration: this._buttonDecoration,
                    )
                ],
            ),
        );
        
        this._pageRoot = new Scaffold(
            body: Container(
                child: Stack(
                    children: <Widget>[
                        Align( // Profile gradient
                            alignment: Alignment.topCenter,
                            child: this._profileBackground,
                        ),
                        Align( // Profile details
                            alignment: Alignment.topCenter,
                            child: this._profileDetailsContainer,
                        ),
                        Align( // Profile picture
                            alignment: Alignment.topCenter,
                            child: this._profilePictureContainer
                        ),
                        Align( // Profile actions
                            alignment: Alignment.center,
                            child: this._profileActions,
                        )
                    ],
                ),
            ),
        );
    }
}