import 'package:Turistas/helpers/CountryStates.dart';
import 'package:Turistas/helpers/User.dart';
import 'package:Turistas/helpers/Validators.dart';
import 'package:Turistas/store/UserProfile.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class EditProfilePage extends StatefulWidget {
    @override
    State<StatefulWidget> createState() => EditProfileState();
}

class EditProfileState extends State<EditProfilePage> {
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    MediaQueryData _deviceDetails;
    Map<String, dynamic> _userProfile = <String, dynamic>{
        "name": "",
        "lastName": "",
        "email": "",
        "birthDate": "",
        "state": ""
    };
    ModelStatesRebuilder _userStore; // User store
    GlobalKey<FormState> _formState = GlobalKey<FormState>();
    GraphQLClient _client;
    /* Widgets */
    Scaffold _rootWidget;
    Container _profileBackground;
    Container _profilePictureContainer; // 
    Container _profileContainer; // Profile container
    Image _profilePicture; // Profile picture
    Form _userProfileForm;
    TextFormField _nameTextField;
    TextFormField _lastNameTextField;
    TextFormField _emailTextField;
    DropdownButtonFormField _state;
    DateTimeField _birthDate;
    MaterialButton _saveChangesButton;
    
    @override
    Widget build(BuildContext context) {
        this._client = GraphQLProvider.of(context).value;
        this._deviceDetails = MediaQuery.of(context);
        
        this._userStore = Injector.getAsModel<UserProfile>(context: context);
        this._userProfile = this._userStore.state.user;
        this._profilePicture = Image.network(this._userProfile["picture"]);
        this._initWidgets(); // init widgets
        
        // print(DateTime.now());
        // print(this._userProfile);
/*         return Injector(
            inject: [
                Inject<UserProfile>(() => UserProfile()),
            ],
            builder: (context, _) {
                return this._rootWidget;
            },
        ); */
        return this._rootWidget;
    }

    InputDecoration _textFieldDecoration({label: String}){
        OutlineInputBorder _borderDecoration = OutlineInputBorder(
            borderSide: BorderSide(
                color: Colors.grey
            )
        );

        return InputDecoration(
            labelText: label,
            enabledBorder: _borderDecoration,
            border: _borderDecoration,
            focusedBorder: _borderDecoration
        );
    }

    List<DropdownMenuItem> _getStatesList(){
        List<DropdownMenuItem<String>> _items = new List();
        
        CountryStates.states.forEach((String _state){
            _items.add(new DropdownMenuItem(
                value: _state,
                child: new Text(_state),
            ));
        });

        return _items;
    }
    void _saveChanges() async {
        // print(this._userProfile);
        if(this._formState.currentState.validate()){
            this._formState.currentState.save();
            // print(this._userProfile);
            QueryResult response = await this._client.mutate(UserHelper.userUpdate(this._userProfile));
            print(response.data["UpdateUser"]);
            
            if(response.data["UpdateUser"]["isSuccess"]){
                // this._userStore.setState((state) => {state = this._userProfile});

                this._scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                        content: Text(
                            response.data["UpdateUser"]["message"],
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18
                            ),
                        ),
                        action: SnackBarAction(
                            label: "CERRAR",
                            onPressed: (){
                                Navigator.pop(context);
                            }
                        )
                    )
                );
            }
        }
    }

    void _initWidgets(){
        this._profileBackground = Container(
            color: Colors.blue,
            height: this._deviceDetails.size.width / 2,
        );
        
        this._profilePictureContainer = Container(
            margin: EdgeInsets.only(
                top: ((this._deviceDetails.size.width / 4) - (this._deviceDetails.size.width / 6))
            ),
            height: this._deviceDetails.size.width / 3,
            width: this._deviceDetails.size.width / 3,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: this._profilePicture,
            )
        );
        
        this._nameTextField = TextFormField(
            decoration: this._textFieldDecoration(label: "Nombre"),
            initialValue: this._userProfile["name"],
            validator: (String name) => ValidatorsHelper.notEmpty(name),
            onSaved: (String name) => this._userProfile["name"] = name,
        );

        this._lastNameTextField = TextFormField(
            decoration: this._textFieldDecoration(label: "Apellidos"),
            initialValue: this._userProfile["lastName"],
            validator: (String lastName) => ValidatorsHelper.notEmpty(lastName),
            onSaved: (String lastName) => this._userProfile["lastName"] = lastName,
        );

        this._birthDate = DateTimeField(
            initialValue: this._userProfile != null ? DateTime.parse(this._userProfile["birthDate"]) : DateTime.now(),
            // initialValue: DateTime.parse("2019-12-04 00:00:00"),
            format: DateFormat("dd-MM-yyyy"),
            onShowPicker: (context, currentValue){
                return showDatePicker(
                    context: context,
                    firstDate: DateTime(1950),
                    initialDate: currentValue ?? DateTime.now(),
                    lastDate: DateTime.now()
                );
            },
            style: TextStyle(
                fontSize: 18
            ),
            decoration: this._textFieldDecoration(label: "Fecha de nacimiento"),
            validator: (DateTime date){
                if(date == null || date.toString() == ""){
                    return "Debes de llenar este campo";
                }
            },
            onSaved: (DateTime value){
                // value.mont
                print(value.toString());
                this._userProfile["birthDate"] = value.toString();
            },
        );

        this._state = DropdownButtonFormField(
            decoration: this._textFieldDecoration(label: "Estado"),
            items: this._getStatesList(),
            onChanged: (dynamic value){
                setState(() {
                    this._userProfile["state"] = value;
                });
            },
            validator: (dynamic value){
                if(value == null || value == ""){
                    return "Debes de llenar este campo";
                }
                else {
                    return null;
                }
            },
            value: this._userProfile["state"]
        );

        this._emailTextField = TextFormField(
            decoration: this._textFieldDecoration(label: "Correo"),
            initialValue: this._userProfile["email"],
            validator: (String email) => ValidatorsHelper.validateEmail(email),
            onSaved: (String email) => this._userProfile["email"] = email,
        );

        this._saveChangesButton = MaterialButton(
            child: Text(
                "Guardar",
                style: TextStyle(
                    color: Colors.white
                ),
            ),
            onPressed: (){
                this._saveChanges();
            },
        );

        this._userProfileForm = Form(
            key: this._formState,
            child: Container(
                margin: EdgeInsets.only( right: 10, left: 10 ),
                child: SingleChildScrollView(
                    child: Column(
                        children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(bottom: 20,top: 50),
                                child: this._nameTextField
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 20),
                                child: this._lastNameTextField,
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 20),
                                child: this._birthDate,
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 20),
                                child: this._state,
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 40),
                                child: this._emailTextField,
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 30),
                                width: this._deviceDetails.size.width - 100,
                                decoration: BoxDecoration(
                                    borderRadius: new BorderRadius.all(Radius.circular(10)),
                                    color: Colors.blueAccent
                                ),
                                child: this._saveChangesButton,
                            )
                        ],
                    ),
                )
            )
        );

        this._profileContainer = Container(
            margin: EdgeInsets.fromLTRB(
                30, this._deviceDetails.size.width / 3,
                30, 30),
            // height: this._deviceDetails.size.height / 1.5,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 5
                    )
                ]
            ),
            child: this._userProfileForm,
        );
    
        this._rootWidget = Scaffold(
            key: this._scaffoldKey,
            body: Stack(
                children: <Widget>[
                    Align(
                        alignment: Alignment.topCenter,
                        child: this._profileBackground,
                    ),
                    Align(
                        alignment: Alignment.topCenter,
                        child: this._profilePictureContainer,
                    ),
                    Align(
                        child: this._profileContainer,
                        alignment: Alignment.center,
                    )
                ],
            )
        );
    }
}