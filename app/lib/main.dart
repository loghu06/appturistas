import 'package:Turistas/auth/signup.page.dart';
import 'package:Turistas/dashboard/PlaceReviews.page.dart';
import 'package:Turistas/dashboard/dashboard.dart';
import 'package:Turistas/dashboard/editProfile.page.dart';
import 'package:Turistas/dashboard/favoritePlaces.dart';
import 'package:Turistas/dashboard/mapa.dart';
import 'package:Turistas/dashboard/writeReview.page.dart';
import 'package:Turistas/graphql/GraphQL.dart';
import 'package:Turistas/helpers/DatabaseHelper.dart';
import 'package:Turistas/helpers/JwtDecoder.dart';
import 'package:Turistas/helpers/StoreHelper.dart';
// import 'package:Turistas/src/providers/Push_notifications_provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'auth/signin.page.dart';

CustomGraphQLClient client = new CustomGraphQLClient();
bool _isExpired = true;

void main() async {
  database.setUpDatabase();
  client.startClient(); // start GraphQL
  _isExpired = JwtHelper.isExpired(await store.getToken()); // Is there a valid token ?
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client.client, // graphql client
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: SignInPage(),
        // home: FavoritePlacesPage(),
        routes: {
          "signIn": (context) => SignInPage(),
          "signUp": (context) => SignUpPage(),
          "dashboard": (context) => Mapa(),
          "editProfile": (context) => EditProfilePage(),
          "favoritePlaces": (context) => FavoritePlacesPage(),
          "placeReviews": (context) => PlaceReviewsPage(),
          "writeReview": (context) => WriteReviewPage()
        },
      ),
    );
  }
}



class Notificaciones extends StatefulWidget {

  @override
  _NotificacionesState createState() => _NotificacionesState();
}

class _NotificacionesState extends State<Notificaciones> {
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // final pushProvider = new PushNotificationsProvider();
    // pushProvider.initNotifications();

  }

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Push Local',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: Center(
          child: Container(
            child: Text('Hello World'),
          ),
        ),
      ),
    );
  }
}



/*
    Clasificaciones de los lugares

    Comida *
      Restaurantes
      café
    Parques *
    Museos *
    Centros comerciales 
    Hoteles 
    Bares *
 */