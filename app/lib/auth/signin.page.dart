import 'package:Turistas/graphql/Types.dart';
import 'package:Turistas/helpers/Auth.dart';
import 'package:Turistas/helpers/JwtDecoder.dart';
import 'package:Turistas/helpers/StoreHelper.dart';
import 'package:Turistas/helpers/Toast.dart';
import 'package:Turistas/helpers/Validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class SignInPage extends StatefulWidget {
    @override
    SignInState createState() => SignInState();
}

class SignInState extends State<SignInPage>{
    final GlobalKey<FormState> _loginKey = GlobalKey<FormState>();
    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    final FacebookLogin _facebookClient = new FacebookLogin(); // Login handler
    GraphQLClient client; // Graphql Client
    LoginData _loginData = new LoginData(); // Email and password
    bool _obscureText = true; // hide password
    bool _isTokenExpired = true; // Session
    String _token = "";

    // Widgets a usar
    MediaQueryData _deviceDetails; // Device details (width, height, etc)
    Text _appName; // App Text
    SignInButton _facebookButton; // Facebook Sign In button
     // Sign in form (Has app name, email, password, facebook's button and  Sign up button)
    Form _signInForm;
    TextFormField _emailTextField; // Email text field
    TextFormField _passwordTextField; // Password text field
    FlatButton _signUpButton; // Go to Sign up view
    Builder _rootWidget;

    @override
    initState(){
        super.initState();
        this._verifyToken();
    }
    
    Widget build(BuildContext context){
        WidgetsBinding.instance.addPostFrameCallback((_) => _initGraphQLClient(context));
        this._deviceDetails = MediaQuery.of(context);
        this._initWidgets();
        
        return Scaffold(
            // backgroundColor: Color.fromARGB(255, 228, 71, 120),
            backgroundColor: Colors.blue,
            body: this._rootWidget
        );
    }

    // Set up GraphQL client
    void _initGraphQLClient(BuildContext context){
        client = GraphQLProvider.of(context).value;
    }

    // Look for a valid JWT
    void _verifyToken() async {
        this._token = await store.getToken(); // Get token from store
        this._isTokenExpired = JwtHelper.isExpired(this._token); // Verify the session. Is a valid token?

        if(!this._isTokenExpired){
            Navigator.pushReplacementNamed(this._scaffoldKey.currentContext, "dashboard");
        }
    }

    // Sign in using email and password
    void _signIn(BuildContext context) async {
        if(this._loginKey.currentState.validate()){
            this._loginKey.currentState.save();
            // Make a query 
            QueryResult result = await this.client.query(AuthHelper.signInOptions(this._loginData));

            // If the email and password are valid then the token has to be saved
            if(result.data["Login"]["isSuccess"]){
                ToastHelper.showToast(context, "Inicio de sesión con exito");
                String token = result.data["Login"]["message"];
                await store.setToken(token); // Save token
                Navigator.pushReplacementNamed(context, "dashboard"); // Go to main view
            }
            else { // If either email or password are invalid then show a message
                ToastHelper.showToast(context, result.data["Login"]["message"]);
            }
        }
    }

    // Sign in using facebook
    void _facebookSignIn(String token) async {
        QueryResult response = await this.client.query(AuthHelper.facebookSignIn(token));
        
        if(response.data["FacebookSignIn"]["isSuccess"]){
            store.setToken(response.data["FacebookSignIn"]["message"]);
            Navigator.pushReplacementNamed(this._scaffoldKey.currentContext, "dashboard");
        }
        else {
            ToastHelper.showToast(this._scaffoldKey.currentContext, response.data["FacebookSignIn"]["message"]);
        }
    }

    // Set up widgets
    void _initWidgets(){
        // App name
        this._appName = Text(
            "App", 
            style: TextStyle(
                color: Colors.white,
                fontSize: 50,
                fontFamily: "DancingScript"
            ),
            textAlign: TextAlign.center,
        );
        // Emai text field
        this._emailTextField = TextFormField(
            keyboardType: TextInputType.emailAddress,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 18,
                color: Colors.white
            ),
            decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                errorBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                errorStyle: TextStyle(color: Colors.white),
                labelText: "Email",
                labelStyle: TextStyle(
                    color: Colors.white
                ),
            ),
            validator: ValidatorsHelper.validateEmail,
            onSaved: (String value) => this._loginData.email = value,
            /* validator: ValidatorsHelper.validateEmail("ksalk"), */
            // validator: Validate.isEmail("input"),
        );
        // Password text field
        this._passwordTextField = TextFormField(
            autocorrect: false,
            textAlign: TextAlign.center,
            obscureText: this._obscureText,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: InputDecoration(
                border: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                enabledBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                focusedBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                errorBorder: const OutlineInputBorder(
                    borderSide: const BorderSide(color: Colors.white)
                ),
                errorStyle: TextStyle(color: Colors.white),
                labelText: "Password",
                labelStyle: TextStyle(
                    color: Colors.white
                ),
                suffixIcon: IconButton(
                    icon: this._obscureText ? 
                        Icon(Icons.remove_red_eye,color: Colors.white) : 
                        Icon(Icons.visibility_off, color: Colors.white),
                    onPressed: (){
                        this.setState((){
                            this._obscureText = !this._obscureText;
                        });
                    },
                )
            ),
            validator: ValidatorsHelper.validatePassword,
            onSaved: (String value) => this._loginData.password = value
        );
        // Facebook button
        this._facebookButton = SignInButton(
            Buttons.Facebook,
            text: "Inicia sesión con Facebook",
            onPressed: () async {
                var user = await this._facebookClient.logIn(["email"]);
                this._facebookSignIn(user.accessToken.token);
            },
        );
        // Sign up button
        this._signUpButton = FlatButton(
            child: Text(
                "¿No tienes una cuenta? Registro",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),
            ),
            onPressed: (){
                Navigator.pushNamed(context, "signUp");
            },
        );
        // Sign in form
        this._signInForm = Form(
            key: _loginKey, // Form key
            child: ListView(
                children: <Widget>[
                    this._appName,
                    Divider(color: Color.fromARGB(0,0,0,0), height: 32),
                    this._emailTextField,
                    Divider(color: Color.fromARGB(0,0,0,0)),
                    this._passwordTextField,
                    Divider(color: Color.fromARGB(0,0,0,0)),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                            this._facebookButton,
                            Container(
                                margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                                child: this._signUpButton
                            )
                        ],
                    )
                ],
            ),
        );
        // Root widget
        this._rootWidget = Builder(
            key: this._scaffoldKey,
            builder: (scaffoldContext){
                return Stack(
                    children: <Widget>[
                        Align(
                            alignment: Alignment.center,
                            child: Container(
                                margin: EdgeInsets.fromLTRB(30, 24, 30, 50),
                                height: MediaQuery.of(context).size.height / 1.2,
                                child: this._signInForm
                            ),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: MaterialButton(
                                child: Text(
                                    "INICIAR SESIÓN", 
                                    style: TextStyle(
                                        fontSize: 18
                                    )
                                ),
                                minWidth: this._deviceDetails.size.width,
                                height: 50,
                                // color: Color.fromARGB(255, 226, 51, 100),
                                color: Colors.blue[800],
                                textColor: Colors.white,
                                onPressed: (){
                                    this._signIn(this._scaffoldKey.currentContext);
                                }
                            )
                        )
                    ],
                );
            },
        );
    }
}