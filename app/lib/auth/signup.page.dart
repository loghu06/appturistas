import 'package:Turistas/graphql/Types.dart';
import 'package:Turistas/helpers/Auth.dart';
import 'package:Turistas/helpers/CountryStates.dart';
import 'package:Turistas/helpers/StoreHelper.dart';
import 'package:Turistas/helpers/Toast.dart';
import 'package:Turistas/helpers/Validators.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class SignUpPage extends StatefulWidget {
    @override
    SignUpState createState() => SignUpState();
}

class SignUpState extends State<SignUpPage>{
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    final GlobalKey<FormState> _signUpKey = GlobalKey<FormState>();
    final GlobalKey<FormFieldState> _passwordConfKey = GlobalKey<FormFieldState>();
    final FacebookLogin _facebookClient = new FacebookLogin(); // Facebook OAuth2
    GraphQLClient _client;
    bool _obscureText = true;
    SignUpData _signUpData = new SignUpData();
    
    // Widgets a usar
    MediaQueryData _deviceDetails;
    Text _appName;
    SignInButton _facebookButton; // Facebook Sign Up button
     // Sign in form (Has app name, name, lastName, email, password, passwordConfirmation
     // facebook's button, Sign in button )
    Form _signUpForm;
    // InputDecoration _textFieldDecoration;
    TextFormField _nameTextField;
    TextFormField _lastNameTextField;
    DateTimeField _birthDate;
    DropdownButtonFormField _state;
    TextFormField _emailTextField; // Email text field
    TextFormField _passwordTextField; // Password text field
    TextFormField _passwordConfirmTextField;
    FlatButton _signInButton; // Go to Sign In view
    Builder _rootWidget;

    void _initGraphQLClient(BuildContext context){
        _client = GraphQLProvider.of(context).value;
    }

    @override
    initState(){
        super.initState();
    }

    Widget build(BuildContext context){
        WidgetsBinding.instance.addPostFrameCallback((_) => this._initGraphQLClient(context));
        this._initWidgets();

        return Scaffold(
            // backgroundColor: Color.fromARGB(255, 228, 71, 120),
            backgroundColor: Colors.blue,
            body: this._rootWidget,
        );
    }

    void _signUp(BuildContext context) async {
        if(this._signUpKey.currentState.validate()){
            // print(this._signUpData.getData());
            QueryResult result = await this._client.mutate(AuthHelper.signUpOptions(this._signUpData));
            // print(result.errors);
            if(result.data["User"]["isSuccess"]){
                // save token and go to favorite places
                String token = result.data["User"]["token"];
                await store.setToken(token); // Save token
                Navigator.pushReplacementNamed(context, "dashboard"); // Go to main view
            }
            else {
                ToastHelper.showToast(context, result.data["User"]["message"]);
            }
        }
    }

    void _facebookSignUp(String token) async {
        QueryResult response = await this._client.mutate(AuthHelper.facebookSignUp(token));

        if(response.data["FacebookSignUp"]["isSuccess"]){
            store.setToken(response.data["FacebookSignUp"]["message"]);
            Navigator.pushReplacementNamed(this._scaffoldKey.currentContext, "dashboard");
        }
        else {
            ToastHelper.showToast(this._scaffoldKey.currentContext, response.data["FacebookSignUp"]["message"]);
        }
    }

    InputDecoration _passwordFieldDecoration({String labelText}){
        return this._textFieldDecoration(
            labelText: labelText,
            suffixIcon: IconButton(
                icon: this._obscureText ? 
                    Icon(Icons.remove_red_eye, color: Colors.white) :
                    Icon(Icons.visibility_off, color: Colors.white),
                onPressed: (){
                    setState(() {
                        this._obscureText = !this._obscureText;
                    });
                },
            )
        );
    }

    InputDecoration _textFieldDecoration({
        String labelText,
        IconButton suffixIcon
    }){
        return InputDecoration(
            enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white)
            ),
            border: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white)
            ),
            focusedBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white)
            ),
            errorBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.white)
            ),
            errorStyle: TextStyle(color: Colors.white),
            labelStyle: TextStyle(
                color: Colors.white, fontSize: 18
            ),
            labelText: labelText,
            suffixIcon: suffixIcon
        );
    }

    // Get all states
    List<DropdownMenuItem> _getStatesList(){
        List<DropdownMenuItem<String>> _items = new List();
        
        CountryStates.states.forEach((String _state){
            _items.add(new DropdownMenuItem(
                value: _state,
                child: new Text(_state),
            ));
        });

        return _items;
    }

    void _initWidgets(){
        // App name
        this._appName = Text(
            "App",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 50,
                fontFamily: "DancingScript",
                color: Colors.white
            ),
        );
        // Name text field
        this._nameTextField = TextFormField(
            /* textAlign: TextAlign.center, */
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: this._textFieldDecoration(labelText: "Nombre"),
            // validator: ValidatorsHelper.notEmpty,
            validator: (String value){
                return ValidatorsHelper.notEmpty(value);
            },
            onSaved: (String value){
                this._signUpData.name = value;
            },
        );
        //Birth date
        this._birthDate = DateTimeField(
            format: DateFormat("dd-MM-yyyy"),
            onShowPicker: (context, currentValue){
                return showDatePicker(
                    context: context,
                    firstDate: DateTime(1950),
                    initialDate: currentValue ?? DateTime.now(),
                    lastDate: DateTime.now()
                );
            },
            /* textAlign: TextAlign.center, */
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: this._textFieldDecoration(labelText: "Fecha de nacimiento"),
            validator: (DateTime date){
                if(date == null || date.toString() == ""){
                    return "Debes de llenar este campo";
                }
            },
            // validator: ValidatorsHelper.notEmpty,
            onSaved: (DateTime value){
                // value.mont
                this._signUpData.birthDate = value.toString();
            },
        );
        // Last name field
        this._lastNameTextField = TextFormField(
            /* textAlign: TextAlign.center, */
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: this._textFieldDecoration(labelText: "Apellidos"),
            validator: ValidatorsHelper.notEmpty,
            onSaved: (String value){
                this._signUpData.lastName = value;
            },
        );
        // State selector
        this._state = DropdownButtonFormField(
            decoration: this._textFieldDecoration(labelText: "Estado"),
            items: this._getStatesList(),
            onChanged: (dynamic value){
                setState(() {
                    this._signUpData.state = value;
                });
            },
            validator: (dynamic value){
                if(value == null || value == ""){
                    return "Debes de llenar este campo";
                }
                else {
                    return null;
                }
            },
            value: this._signUpData.state,
        );
        // Email field
        this._emailTextField = TextFormField(
            keyboardType: TextInputType.emailAddress,
            /* textAlign: TextAlign.center, */
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: this._textFieldDecoration(labelText: "Email"),
            validator: ValidatorsHelper.validateEmail,
            onSaved: (String value){
                this._signUpData.email = value;
            },
        );
        // Password field
        this._passwordTextField = TextFormField(
            /* textAlign: TextAlign.center, */
            obscureText: this._obscureText,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: this._passwordFieldDecoration(labelText: "Contraseña"),
            validator: (String value){
                String passwordMessage = ValidatorsHelper.validatePassword(value);
                String errorMessage = passwordMessage == null ? 
                ValidatorsHelper.comparePassword(value, this._passwordConfKey.currentState.value) : 
                passwordMessage;

                return errorMessage;
            },
            onSaved: (String value){
                this._signUpData.password = value;
            },
        );
        // Password confirmation field
        this._passwordConfirmTextField = TextFormField(
            key: this._passwordConfKey,
            /* textAlign: TextAlign.center, */
            obscureText: this._obscureText, // show / hide password
            style: TextStyle(
                color: Colors.white,
                fontSize: 18
            ),
            decoration: this._passwordFieldDecoration(labelText: "Confirma tu contraseña"),
            validator: ValidatorsHelper.validatePassword,
        );
        // Facebook button
        this._facebookButton = SignInButton(
            Buttons.Facebook,
            text: "Registro con Facebook",
            onPressed: () async {
                FacebookLoginResult user = await this._facebookClient.logIn(["email"]);
                this._facebookSignUp(user.accessToken.token);
            },
        );
        // Sign in button
        this._signInButton = FlatButton(
            child: Text(
                "¿Ya tienes una cuenta? Inicia sesión",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18
                ),
            ),
            onPressed: (){
                Navigator.pop(context);
            },
        );
        //Sign up form
        this._signUpForm = Form(
            key: this._signUpKey,
            child: ListView(
                children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._appName,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._nameTextField,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._lastNameTextField,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._birthDate,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._state,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._emailTextField,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15),
                        child: this._passwordTextField,
                    ),
                    Container(
                        margin: EdgeInsets.only(top: 15, bottom: 15),
                        child: this._passwordConfirmTextField,
                    ),
                    Column(
                        children: <Widget>[
                            this._facebookButton,
                            Container(
                                padding: EdgeInsets.fromLTRB(0, 16, 0, 15),
                                child: this._signInButton,
                            )
                        ],
                    )
                ]
            ),
        );
        // Root widget
        this._rootWidget = Builder(
            key: this._scaffoldKey,
            builder: (BuildContext rootContext){
                return Stack(
                    children: <Widget>[
                        Align(
                            alignment: Alignment.center,
                            child: Container(
                                margin: EdgeInsets.fromLTRB(30, 24, 30, 50),
                                child: this._signUpForm
                            ),
                        ),
                        Align(
                            alignment: Alignment.bottomCenter,
                            child: MaterialButton(
                                child: Text(
                                    "CONTINUAR",
                                    style: TextStyle(fontSize: 18)
                                ),
                                minWidth: MediaQuery.of(context).size.width,
                                    height: 50,
                                    // color: Color.fromARGB(255, 226, 51, 100),
                                    color: Colors.blue[800],
                                    textColor: Colors.white,
                                    onPressed: (){
                                        this._signUpKey.currentState.save();
                                        this._signUp(rootContext);
                                    }
                            ),
                        )
                    ]
                );
            },
        );
    }
}