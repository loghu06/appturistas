class LoginData {
    String email;
    String password;
}

class _User {
    String name;
    String lastName;
    String email;
    String password;
    String birthDate;
    String state;
}

class SignUpData extends _User {
    Map<String, String> getData(){
        return <String, String>{
            "name": this.name,
            "lastName": this.lastName,
            "email": this.email,
            "password": this.password,
            "birthDate": this.birthDate,
            "state": this.state
        };
    }
}

class UserData extends _User {
    String _id;
    String picture;
}