"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const config_1 = require("./config");
const server_1 = require("./server");
const models_1 = require("./models");
console.log("\x1Bc");
function runServer() {
    return __awaiter(this, void 0, void 0, function* () {
        yield Promise.all([
            new server_1.Server(config_1.PORT),
            new models_1.Mongoose(config_1.MONGO_URI, config_1.MONGO_CONFIG)
        ]);
    });
}
runServer();
