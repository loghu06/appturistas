var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const expect = require('chai').expect;
const url = "http://localhost:7000/graphql";
const request = require('supertest')(url);
describe("Admin test", () => {
    let testToken = "";
    // let testEmail = "15690218@tecvalles.mx"
    let testEmail = "admin@admin.com";
    let testPassword = "test00";
    it("Create a new admin", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .send({
                query: `
                    mutation {
                        Admin(
                            admin: {
                                name: "test",
                                lastName: "test",
                                email: "${testEmail}",
                                password: "${testPassword}"
                            }
                        ){
                            message isSuccess
                        }
                    }
                `
            });
        }
        catch (error) { }
        console.log(body.data);
        expect(body.data.Admin.isSuccess).to.equal(true);
    }));
    it("Admin login", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .send({
                query: `
                    {
                        Login(
                            email: "${testEmail}",
                            password: "${testPassword}"
                        ){ message isSuccess }
                    }
                `
            });
        }
        catch (error) { }
        testToken = body.data.Login.message;
        expect(body.data.Login.isSuccess).to.equal(true);
    }));
    it("Update admin", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", testToken)
                .send({
                query: `
                    mutation {
                        UpdateAdmin(
                            admin: {
                                name: "Test name"
                            }
                        ){ message isSuccess }
                    }
                `
            });
        }
        catch (error) { }
        expect(body.data.UpdateAdmin.isSuccess).to.equal(true);
    }));
    it("Get admin data", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", testToken)
                .send({
                query: `
                    { Admin { _id name } }
                `
            });
        }
        catch (error) { }
        console.log(body.data.Admin);
        expect(body.data.Admin).to.have.property("_id");
    }));
    it("Delete admin", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", testToken)
                .send({
                query: ` mutation { DeleteAdmin { isSuccess } } `
            });
        }
        catch (error) { }
        expect(body.data.DeleteAdmin.isSuccess).to.equal(true);
    }));
});
