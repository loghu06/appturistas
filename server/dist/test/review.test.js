var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const expect = require("chai").expect;
const url = "http://localhost:7000/graphql";
const request = require("supertest")(url);
describe("Review testing", () => {
    const userToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWQ1MzgxMDNhMGIwNmI2MDRlNTFiN2Y3IiwiaWF0IjoxNTY2ODQwNzM2LCJleHAiOjE1OTgzOTgzMzZ9.46QD7hxME91qVhCNsGj_8s5d189_o_AqgmeA6gUoQso";
    const testPlace = "5d64198c505a2415820e42e0";
    let testReview = "";
    it("New review", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", userToken)
                .send({
                query: `
                    mutation {
                        Review(
                            review: {
                                place: "${testPlace}",
                                rating: 5,
                                message: "An awesome place"
                            }
                        ){ message isSuccess }
                    }
                `
            });
        }
        catch (error) { }
        console.log(body);
        testReview = body.data.Review.message;
        expect(body.data.Review.isSuccess).to.equal(true);
    }));
    it("Update review", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", userToken)
                .send({
                query: `
                    mutation {
                        UpdateReview(
                            review: {
                                _id: "${testReview}",
                                message: "Updated review",
                                place: "${testPlace}",
                                rating: 5
                            }
                        ){ isSuccess message }
                    }
                `
            });
        }
        catch (error) { }
        expect(body.data.UpdateReview.isSuccess).to.equal(true);
    }));
    it("Remove review", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", userToken)
                .send({
                query: `
                    mutation {
                        DeleteReview(id: "${testReview}"){ isSuccess }
                    }
                `
            });
        }
        catch (error) { }
        console.log(body);
        expect(body.data.DeleteReview.isSuccess).to.equal(true);
    }));
    it("Get place reviews", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", userToken)
                .send({
                query: `
                    {
                        PlaceReviews(place: "${testPlace}"){ _id message }
                    }
                `
            });
        }
        catch (error) { }
        console.log(body.data.PlaceReviews);
        expect(body.data.PlaceReviews.length).to.be.above(0);
    }));
    it("Get user reviews", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post("")
                .set("token", userToken)
                .send({
                query: `
                    {
                        UserReviews{ _id message }
                    }
                `
            });
        }
        catch (error) { }
        console.log(body.data.UserReviews);
        expect(body.data.UserReviews.length).to.be.above(0);
    }));
});
