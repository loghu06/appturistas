var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const chai = require('chai');
const expect = chai.expect;
const url = "http://localhost:9008";
const request = require('supertest')(url);
describe('Hello World test', () => {
    it("Returns a Hello World :D", () => __awaiter(this, void 0, void 0, function* () {
        try {
            var { body } = yield request.post('/graphql').send({ query: "{ Hello }" });
        }
        catch (error) { }
        expect(body.data).to.have.property("Hello");
    }));
});
