"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typegoose_1 = require("typegoose");
const User_1 = require("./User");
const mongoose_1 = __importDefault(require("mongoose"));
const jsonwebtoken_1 = require("jsonwebtoken");
const config_1 = require("../config");
class Admin extends User_1.User {
    generateToken() {
        return jsonwebtoken_1.sign({
            admin: this._id
        }, config_1.SECRET_KEY, {
            expiresIn: '1y'
        });
    }
}
__decorate([
    typegoose_1.prop({ default: true, required: true }),
    __metadata("design:type", Boolean)
], Admin.prototype, "isAdmin", void 0);
__decorate([
    typegoose_1.instanceMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], Admin.prototype, "generateToken", null);
exports.default = new Admin().setModelForClass(Admin, {
    existingMongoose: mongoose_1.default,
    schemaOptions: { collection: 'admins' }
});
