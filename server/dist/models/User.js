"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typegoose_1 = require("typegoose");
const bcrypt_1 = require("bcrypt");
const jsonwebtoken_1 = require("jsonwebtoken");
const config_1 = require("../config");
let User = class User extends typegoose_1.Typegoose {
    comparePassword(password) {
        return bcrypt_1.compareSync(password, this.password);
    }
    generateToken() {
        return jsonwebtoken_1.sign({
            user: this._id
        }, config_1.SECRET_KEY, {
            expiresIn: '1y'
        });
    }
    activationLink() {
        // Activation link, valid for 30 minutes
        return jsonwebtoken_1.sign({
            _id: this._id
        }, config_1.SECRET_KEY, {
            expiresIn: "30m"
        });
    }
};
__decorate([
    typegoose_1.prop({ required: true }) // User's name
    ,
    __metadata("design:type", String)
], User.prototype, "name", void 0);
__decorate([
    typegoose_1.prop({ required: true }) // User's lastname
    ,
    __metadata("design:type", String)
], User.prototype, "lastName", void 0);
__decorate([
    typegoose_1.prop({ required: true, unique: true }) // Valid email
    ,
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    typegoose_1.prop({ required: true }) // User's password
    ,
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    typegoose_1.prop({ required: false, unique: false }) // Facebook's id for login
    ,
    __metadata("design:type", String)
], User.prototype, "facebookID", void 0);
__decorate([
    typegoose_1.prop({ required: true }) // Some picture
    ,
    __metadata("design:type", String)
], User.prototype, "picture", void 0);
__decorate([
    typegoose_1.prop({ required: true }),
    __metadata("design:type", String)
], User.prototype, "birthDate", void 0);
__decorate([
    typegoose_1.prop({ required: true }),
    __metadata("design:type", String)
], User.prototype, "state", void 0);
__decorate([
    typegoose_1.arrayProp({ itemsRef: 'KindOfPlace', default: [] }) // Favorite places
    ,
    __metadata("design:type", Array)
], User.prototype, "favoritePlaces", void 0);
__decorate([
    typegoose_1.prop({ required: true, default: false }) // Is not active until the email is verified
    ,
    __metadata("design:type", Boolean)
], User.prototype, "isActive", void 0);
__decorate([
    typegoose_1.instanceMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Boolean)
], User.prototype, "comparePassword", null);
__decorate([
    typegoose_1.instanceMethod // Create a token
    ,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], User.prototype, "generateToken", null);
__decorate([
    typegoose_1.instanceMethod,
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], User.prototype, "activationLink", null);
User = __decorate([
    typegoose_1.pre('findOneAndUpdate', function () {
        if (this._update.password) {
            this._update.password = bcrypt_1.hashSync(this._update.password, config_1.ROUNDS);
        }
    }),
    typegoose_1.pre('save', function (next) {
        if (this.isModified('password')) {
            this.password = bcrypt_1.hashSync(this.password, config_1.ROUNDS);
        }
        next();
    })
], User);
exports.User = User;
exports.default = new User().getModelForClass(User);
