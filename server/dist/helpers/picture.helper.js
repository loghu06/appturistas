"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ui_avatars_1 = require("ui-avatars");
function generatePicture(name) {
    return ui_avatars_1.generateAvatar({
        name,
        uppercase: true,
        background: "000000",
        color: "FFFFFF",
        fontsize: 0.5,
        bold: true,
        length: 2,
        rounded: true,
        size: 250
    });
}
exports.generatePicture = generatePicture;
