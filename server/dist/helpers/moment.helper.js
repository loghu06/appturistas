"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const moment_1 = __importDefault(require("moment"));
moment_1.default.locale("es", {
    months: [
        "Enero", "Febrero", "Marzo", "Abril",
        "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ],
    monthsShort: [
        "En", "Feb", "Mar", "Abr",
        "May", "Jun", "Jul", "Ag",
        "Sep", "Oct", "Nov", "Dic"
    ],
    weekdays: [
        "Domingo", "Lunes", "Martes", "Miercoles",
        "Jueves", "Viernes", "Sabado"
    ],
    weekdaysMin: [
        "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"
    ],
    weekdaysShort: [
        "Dom", "Lun", "Mar", "Mi", "Ju", "Vi", "Sa"
    ]
});
exports.default = moment_1.default;
