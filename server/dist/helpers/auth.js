"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = require("jsonwebtoken");
const config_1 = require("../config");
// Tries to decode some token. If it's a valid token then return the data
// otherwise return false which means that is not a valid token.
function decodeToken(token) {
    try {
        return jsonwebtoken_1.verify(token, config_1.SECRET_KEY);
    }
    catch (error) {
        return false; // Invalid or expired token
    }
}
exports.decodeToken = decodeToken;
