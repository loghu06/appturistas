"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const class_validator_1 = require("class-validator");
const KindOfPlace_type_1 = require("./KindOfPlace.type");
let Place = class Place {
};
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID),
    __metadata("design:type", String)
], Place.prototype, "_id", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Place.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float),
    __metadata("design:type", Number)
], Place.prototype, "longitude", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float),
    __metadata("design:type", Number)
], Place.prototype, "latitude", void 0);
__decorate([
    type_graphql_1.Field(type => KindOfPlace_type_1.KindOfPlace),
    __metadata("design:type", KindOfPlace_type_1.KindOfPlace)
], Place.prototype, "kindOfPlace", void 0);
__decorate([
    type_graphql_1.Field(type => [String], { nullable: "itemsAndList" }),
    __metadata("design:type", Array)
], Place.prototype, "pictures", void 0);
Place = __decorate([
    type_graphql_1.ObjectType()
], Place);
exports.Place = Place;
let PlaceInput = class PlaceInput {
};
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID, { nullable: true }),
    __metadata("design:type", String)
], PlaceInput.prototype, "_id", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    class_validator_1.MinLength(1),
    __metadata("design:type", String)
], PlaceInput.prototype, "name", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float, { nullable: true }),
    __metadata("design:type", Number)
], PlaceInput.prototype, "longitude", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float, { nullable: true }),
    __metadata("design:type", Number)
], PlaceInput.prototype, "latitude", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID, { nullable: true }),
    __metadata("design:type", KindOfPlace_type_1.KindOfPlace)
], PlaceInput.prototype, "kindOfPlace", void 0);
__decorate([
    type_graphql_1.Field(type => [String], { nullable: "itemsAndList" }),
    __metadata("design:type", Array)
], PlaceInput.prototype, "pictures", void 0);
PlaceInput = __decorate([
    type_graphql_1.InputType()
], PlaceInput);
exports.PlaceInput = PlaceInput;
