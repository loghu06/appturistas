"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const Place_type_1 = require("./Place.type");
const User_type_1 = require("./User.type");
let Review = class Review {
};
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID),
    __metadata("design:type", String)
], Review.prototype, "_id", void 0);
__decorate([
    type_graphql_1.Field(type => User_type_1.User),
    __metadata("design:type", User_type_1.User)
], Review.prototype, "user", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Review.prototype, "message", void 0);
__decorate([
    type_graphql_1.Field(type => Place_type_1.Place),
    __metadata("design:type", Place_type_1.Place)
], Review.prototype, "place", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Review.prototype, "date", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    __metadata("design:type", Number)
], Review.prototype, "rating", void 0);
Review = __decorate([
    type_graphql_1.ObjectType()
], Review);
exports.Review = Review;
let ReviewInput = class ReviewInput {
};
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID, { nullable: true }),
    __metadata("design:type", String)
], ReviewInput.prototype, "_id", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID, { nullable: true }),
    __metadata("design:type", User_type_1.User)
], ReviewInput.prototype, "user", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], ReviewInput.prototype, "message", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.ID),
    __metadata("design:type", Place_type_1.Place)
], ReviewInput.prototype, "place", void 0);
__decorate([
    type_graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], ReviewInput.prototype, "date", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    __metadata("design:type", Number)
], ReviewInput.prototype, "rating", void 0);
ReviewInput = __decorate([
    type_graphql_1.InputType()
], ReviewInput);
exports.ReviewInput = ReviewInput;
