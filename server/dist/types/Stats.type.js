"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
let Stats = class Stats {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Stats.prototype, "state", void 0);
__decorate([
    type_graphql_1.Field(type => [PlaceData]),
    __metadata("design:type", Array)
], Stats.prototype, "data", void 0);
Stats = __decorate([
    type_graphql_1.ObjectType()
], Stats);
exports.Stats = Stats;
let PlaceData = class PlaceData {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], PlaceData.prototype, "kind", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], PlaceData.prototype, "ageRange", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    __metadata("design:type", Number)
], PlaceData.prototype, "users", void 0);
PlaceData = __decorate([
    type_graphql_1.ObjectType()
], PlaceData);
/*

[
  {
    '$project': {
      '_id': 1,
      'place': {
        '$toObjectId': '$place'
      },
      'rating': 1,
      'user': {
        '$toObjectId': '$user'
      },
      'date': 1
    }
  }, {
    '$lookup': {
      'from': 'places',
      'localField': 'place',
      'foreignField': '_id',
      'as': 'place'
    }
  }, {
    '$lookup': {
      'from': 'users',
      'localField': 'user',
      'foreignField': '_id',
      'as': 'user'
    }
  }, {
    '$unwind': {
      'path': '$place'
    }
  }, {
    '$unwind': {
      'path': '$user'
    }
  }, {
    '$project': {
      '_id': 1,
      'rating': 1,
      'date': 1,
      'place': 1,
      'user': 1,
      'birthDate': {
        '$dateFromString': {
          'dateString': '$user.birthDate'
        }
      },
      'kindOfPlace': {
        '$toObjectId': '$place.kindOfPlace'
      }
    }
  }, {
    '$lookup': {
      'from': 'kindofplaces',
      'localField': 'kindOfPlace',
      'foreignField': '_id',
      'as': 'kindOfPlace'
    }
  }, {
    '$unwind': {
      'path': '$kindOfPlace'
    }
  }, {
    '$project': {
      'rating': 1,
      'date': 1,
      'place': 1,
      'user': 1,
      'kindOfPlace': '$kindOfPlace.kind',
      'yearBorn': {
        '$year': '$birthDate'
      }
    }
  }, {
    '$project': {
      '_id': 1,
      'rating': 1,
      'place': 1,
      'user': 1,
      'kindOfPlace': 1,
      'date': 1,
      'ageRange': {
        '$switch': {
          'branches': [
            {
              'case': {
                '$gt': [
                  '$yearBorn', 2001
                ]
              },
              'then': 'Menor de 18'
            }, {
              'case': {
                '$gte': [
                  '$yearBorn', 1996
                ]
              },
              'then': 'Entre 18 y 23'
            }, {
              'case': {
                '$gte': [
                  '$yearBorn', 1984
                ]
              },
              'then': 'Entre 24 y 35'
            }, {
              'case': {
                '$gte': [
                  '$yearBorn', 1974
                ]
              },
              'then': 'Entre 36 y 45'
            }, {
              'case': {
                '$lte': [
                  '$yearBorn', 1975
                ]
              },
              'then': 'Mayor de 45'
            }
          ]
        }
      }
    }
  }, {
    '$group': {
      '_id': {
        'state': '$user.state',
        'kind': '$kindOfPlace',
        'ageRange': '$ageRange'
      },
      'users': {
        '$addToSet': '$user'
      },
      'ageRange': {
        '$addToSet': '$ageRange'
      }
    }
  }, {
    '$unwind': {
      'path': '$ageRange'
    }
  }, {
    '$project': {
      '_id': 0,
      'state': '$_id.state',
      'kindOfPlace': '$_id.kind',
      'ageRange': 1,
      'users': {
        '$size': '$users'
      }
    }
  }, {
    '$group': {
      '_id': '$state',
      'data': {
        '$addToSet': {
          '$switch': {
            'branches': [
              {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Museum'
                  ]
                },
                'then': {
                  'kind': 'Museum',
                  'ageRange': '$ageRange',
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Coffee'
                  ]
                },
                'then': {
                  'kind': 'Coffee',
                  'ageRange': '$ageRange',
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Park'
                  ]
                },
                'then': {
                  'kind': 'Park',
                  'ageRange': '$ageRange',
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Bar'
                  ]
                },
                'then': {
                  'kind': 'Bar',
                  'ageRange': '$ageRange',
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Food'
                  ]
                },
                'then': {
                  'kind': 'Food',
                  'ageRange': '$ageRange',
                  'users': '$users'
                }
              }
            ]
          }
        }
      }
    }
  }, {
    '$project': {
      '_id': 0,
      'state': '$_id',
      'data': 1
    }
  }
]

*/ 
