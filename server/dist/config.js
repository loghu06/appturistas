"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
// export const MONGO_URI: string = "mongodb://localhost:27017/admin"
exports.MONGO_URI = "mongodb+srv://devcluster-ue8gw.mongodb.net/admin";
exports.MONGO_CONFIG = {
    user: "testing",
    pass: "NCVrokYrQwhXBxuh",
    dbName: "tourists",
    useNewUrlParser: true,
    useFindAndModify: true,
    useCreateIndex: true
};
exports.ROUNDS = 10;
exports.PORT = parseInt(process.env.PORT) || 7000;
// A file with the key can be used
exports.SECRET_KEY = fs_1.readFileSync('keyfile', 'utf-8');
