"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Place_1 = __importDefault(require("../models/Place"));
const apollo_server_koa_1 = require("apollo-server-koa");
const typedi_1 = require("typedi");
let PlaceService = class PlaceService {
    // Get a place by id
    getPlace(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield Place_1.default.findById(id);
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Get all places
    getPlaces() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield Place_1.default.find().populate({
                    path: "kindOfPlace",
                    model: "KindOfPlace"
                });
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Create a place
    createPlace(placeBody) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const place = yield new Place_1.default(placeBody).save();
                return {
                    message: "Lugar creado con exito", isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Update a place
    updatePlace(placeBody) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Place_1.default.findByIdAndUpdate(placeBody._id, placeBody);
                return {
                    message: "Lugar actualizado", isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Delete a place
    deletePlace(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Place_1.default.findByIdAndRemove(id);
                return {
                    message: "Lugar eliminado", isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
};
PlaceService = __decorate([
    typedi_1.Service()
], PlaceService);
exports.PlaceService = PlaceService;
