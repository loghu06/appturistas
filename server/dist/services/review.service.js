"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Review_1 = __importDefault(require("../models/Review"));
const apollo_server_koa_1 = require("apollo-server-koa");
const moment_helper_1 = __importDefault(require("../helpers/moment.helper"));
const typedi_1 = require("typedi");
let ReviewService = class ReviewService {
    // Get all reviews of a place
    getReviews(placeID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield Review_1.default.find({ place: placeID })
                    .populate({
                    path: "user",
                    model: "User"
                });
            }
            catch (error) {
                console.log(error);
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Get all reviews of a user
    getUserReviews(user) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield Review_1.default.find({ user: user });
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Get a user review 
    getReview(placeID, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield Review_1.default.findOne({
                    place: placeID,
                    user: userID
                });
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Create a review
    createReview(reviewBody) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                reviewBody.date = moment_helper_1.default(new Date()).format("MMMM DD, YYYY HH:mm");
                let review = yield new Review_1.default(reviewBody).save();
                return {
                    message: "Comentario fue creado con exito",
                    isSuccess: true
                };
            }
            catch (error) {
                console.log(error);
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Update a review
    updateReview(reviewBody, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let review = yield Review_1.default.findOne({ _id: reviewBody._id, user: userID });
                if (review) {
                    yield review.updateOne(reviewBody);
                    return {
                        message: "Comentario actualizado", isSuccess: true
                    };
                }
                else {
                    return {
                        message: "No puedes editar este comentario", isSuccess: false
                    };
                }
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Delete a review (By review id and user id to ensure the review belongs 
    // to the user who wants to delete it)
    deleteReview(reviewID, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let review = yield Review_1.default.findOne({ _id: reviewID, user: userID });
                if (review) {
                    yield review.remove();
                    return {
                        message: "Comentario eliminado", isSuccess: true
                    };
                }
                else {
                    return {
                        message: "No puedes eliminar este comentario", isSuccess: false
                    };
                }
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
};
ReviewService = __decorate([
    typedi_1.Service()
], ReviewService);
exports.ReviewService = ReviewService;
