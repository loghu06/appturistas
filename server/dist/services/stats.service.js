"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const apollo_server_koa_1 = require("apollo-server-koa");
const Review_1 = __importDefault(require("../models/Review"));
const moment = require("moment");
let StatsService = class StatsService {
    getStats() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const currentData = moment();
                // Menor de 18
                const lessThan18 = currentData.subtract(18, "years").year();
                // Entre 18 y 23
                const between18and23 = currentData.subtract(5, "years").year();
                // Entre 24 y 35
                const between24and35 = currentData.subtract(12, "years").year();
                // Entre 36 y 45
                const between36and45 = currentData.subtract(10, "years").year();
                // Mayor de 45
                const greaterThan45 = currentData.subtract(1, "year").year();
                return yield Review_1.default.aggregate([
                    {
                        '$project': {
                            '_id': 1,
                            'place': {
                                '$toObjectId': '$place'
                            },
                            'rating': 1,
                            'user': {
                                '$toObjectId': '$user'
                            },
                            'date': 1
                        }
                    }, {
                        '$lookup': {
                            'from': 'places',
                            'localField': 'place',
                            'foreignField': '_id',
                            'as': 'place'
                        }
                    }, {
                        '$lookup': {
                            'from': 'users',
                            'localField': 'user',
                            'foreignField': '_id',
                            'as': 'user'
                        }
                    }, {
                        '$unwind': {
                            'path': '$place'
                        }
                    }, {
                        '$unwind': {
                            'path': '$user'
                        }
                    }, {
                        '$project': {
                            '_id': 1,
                            'rating': 1,
                            'date': 1,
                            'place': 1,
                            'user': 1,
                            'birthDate': {
                                '$dateFromString': {
                                    'dateString': '$user.birthDate'
                                }
                            },
                            'kindOfPlace': {
                                '$toObjectId': '$place.kindOfPlace'
                            }
                        }
                    }, {
                        '$lookup': {
                            'from': 'kindofplaces',
                            'localField': 'kindOfPlace',
                            'foreignField': '_id',
                            'as': 'kindOfPlace'
                        }
                    }, {
                        '$unwind': {
                            'path': '$kindOfPlace'
                        }
                    }, {
                        '$project': {
                            'rating': 1,
                            'date': 1,
                            'place': 1,
                            'user': 1,
                            'kindOfPlace': '$kindOfPlace.kind',
                            'yearBorn': {
                                '$year': '$birthDate'
                            }
                        }
                    }, {
                        '$project': {
                            '_id': 1,
                            'rating': 1,
                            'place': 1,
                            'user': 1,
                            'kindOfPlace': 1,
                            'date': 1,
                            'ageRange': {
                                '$switch': {
                                    'branches': [
                                        {
                                            'case': {
                                                '$gt': [
                                                    '$yearBorn', lessThan18
                                                ]
                                            },
                                            'then': 'Menor de 18'
                                        }, {
                                            'case': {
                                                '$gte': [
                                                    '$yearBorn', between18and23
                                                ]
                                            },
                                            'then': 'Entre 18 y 23'
                                        }, {
                                            'case': {
                                                '$gte': [
                                                    '$yearBorn', between24and35
                                                ]
                                            },
                                            'then': 'Entre 24 y 35'
                                        }, {
                                            'case': {
                                                '$gte': [
                                                    '$yearBorn', between36and45
                                                ]
                                            },
                                            'then': 'Entre 36 y 45'
                                        }, {
                                            'case': {
                                                '$lte': [
                                                    '$yearBorn', greaterThan45
                                                ]
                                            },
                                            'then': 'Mayor de 45'
                                        }
                                    ]
                                }
                            }
                        }
                    }, {
                        '$group': {
                            '_id': {
                                'state': '$user.state',
                                'kind': '$kindOfPlace',
                                'ageRange': '$ageRange'
                            },
                            'users': {
                                '$addToSet': '$user'
                            },
                            'ageRange': {
                                '$addToSet': '$ageRange'
                            }
                        }
                    }, {
                        '$unwind': {
                            'path': '$ageRange'
                        }
                    }, {
                        '$project': {
                            '_id': 0,
                            'state': '$_id.state',
                            'kindOfPlace': '$_id.kind',
                            'ageRange': 1,
                            'users': {
                                '$size': '$users'
                            }
                        }
                    }, {
                        '$group': {
                            '_id': '$state',
                            'data': {
                                '$addToSet': {
                                    '$switch': {
                                        'branches': [
                                            {
                                                'case': {
                                                    '$eq': [
                                                        '$kindOfPlace', 'Museum'
                                                    ]
                                                },
                                                'then': {
                                                    'kind': 'Museos',
                                                    'ageRange': '$ageRange',
                                                    'users': '$users'
                                                }
                                            }, {
                                                'case': {
                                                    '$eq': [
                                                        '$kindOfPlace', 'Coffee'
                                                    ]
                                                },
                                                'then': {
                                                    'kind': 'Cafeterias',
                                                    'ageRange': '$ageRange',
                                                    'users': '$users'
                                                }
                                            }, {
                                                'case': {
                                                    '$eq': [
                                                        '$kindOfPlace', 'Park'
                                                    ]
                                                },
                                                'then': {
                                                    'kind': 'Parques',
                                                    'ageRange': '$ageRange',
                                                    'users': '$users'
                                                }
                                            }, {
                                                'case': {
                                                    '$eq': [
                                                        '$kindOfPlace', 'Bar'
                                                    ]
                                                },
                                                'then': {
                                                    'kind': 'Bares',
                                                    'ageRange': '$ageRange',
                                                    'users': '$users'
                                                }
                                            }, {
                                                'case': {
                                                    '$eq': [
                                                        '$kindOfPlace', 'Food'
                                                    ]
                                                },
                                                'then': {
                                                    'kind': 'Comida',
                                                    'ageRange': '$ageRange',
                                                    'users': '$users'
                                                }
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    }, {
                        '$project': {
                            '_id': 0,
                            'state': '$_id',
                            'data': 1
                        }
                    }
                ]);
            }
            catch (error) {
                console.log(error);
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
};
StatsService = __decorate([
    typedi_1.Service()
], StatsService);
exports.StatsService = StatsService;
