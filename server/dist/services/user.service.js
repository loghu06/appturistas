"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const User_1 = __importDefault(require("../models/User")); // User model
const picture_helper_1 = require("../helpers/picture.helper");
const apollo_server_koa_1 = require("apollo-server-koa");
const email_helper_1 = require("../helpers/email.helper");
const typedi_1 = require("typedi");
let UserService = class UserService {
    constructor() {
        this.facebookUrl = "https://graph.facebook.com/v2.12/me?fields=id,first_name,last_name,email,picture.type(large)&access_token=";
    }
    // Create a user
    createUser(userBody) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user = new User_1.default(userBody);
                user.picture = picture_helper_1.generatePicture(`${user.name} ${user.lastName}`);
                yield user.save();
                return {
                    message: "Usuario registrado",
                    isSuccess: true,
                    token: user.generateToken()
                };
            }
            catch (error) {
                console.log(error);
                if (error.code == 11000) {
                    return {
                        message: "Email ya esta en uso",
                        isSuccess: false
                    };
                }
                else {
                    return {
                        message: "Ocurrio un error",
                        isSuccess: false
                    };
                }
            }
        });
    }
    // Sign in handler
    login(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let user = yield User_1.default.findOne({ email });
                if (user) {
                    if (user.comparePassword(password)) {
                        return {
                            message: user.generateToken(),
                            isSuccess: true
                        };
                    }
                    else {
                        return {
                            message: "Contraseña invalida",
                            isSuccess: false
                        };
                    }
                }
                else {
                    return {
                        message: "Email invalido",
                        isSuccess: false
                    };
                }
            }
            catch (error) {
                return { message: "Ocurrio un error", isSuccess: false };
            }
        });
    }
    // Sign in via Facebook
    facebookSignIn(token) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield axios_1.default({
                    method: "GET",
                    url: this.facebookUrl + token
                });
                // Find a user by facebook's id
                const user = yield User_1.default.findOne({ facebookID: data.data.id });
                if (user) {
                    return {
                        message: user.generateToken(),
                        isSuccess: true
                    };
                }
                else {
                    return {
                        message: "Usuario no se encuentra registrado",
                        isSuccess: false
                    };
                }
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Sign up via facebook
    facebookSignUp(token) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const data = yield axios_1.default({
                    method: "GET",
                    url: this.facebookUrl + token
                });
                const user = yield this.createViaFacebookAccount(data);
                return {
                    message: user.generateToken(),
                    isSuccess: true
                };
            }
            catch (error) {
                if (error.code == 11000) {
                    console.log(error);
                    return {
                        message: "Usuario ya se encuentra registrado",
                        isSuccess: false
                    };
                }
                else {
                    throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
                }
            }
        });
    }
    // Get a user by id (Id is provided by a token)
    findUserById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield User_1.default.findById(id).populate({
                    path: "favoritePlaces",
                    model: "KindOfPlace"
                });
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Update a user
    updateUser(userBody, userID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield User_1.default.findByIdAndUpdate(userID, userBody);
                return {
                    message: "Usuario actualizado",
                    isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Delete a user
    deleteUser(userID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield User_1.default.findByIdAndRemove(userID);
                return {
                    message: "Usuario eliminado",
                    isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Send activation token to user's email
    sendActivation(userID) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield User_1.default.findById(userID);
                const token = user.activationLink();
                email_helper_1.mailer.sendMail({
                    to: user.email,
                    subject: "Confirmacion de la cuenta",
                    html: `<a href="http://localhost:9008/activate-account/${token}">Click para activar cuenta!</a>`
                });
                return {
                    message: "Correo de activacion fue enviado",
                    isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    createViaFacebookAccount({ data }) {
        return __awaiter(this, void 0, void 0, function* () {
            const picture = picture_helper_1.generatePicture(`${data.first_name} ${data.last_name}`);
            return yield new User_1.default({
                facebookID: data.id,
                name: data.first_name,
                lastName: data.last_name,
                picture,
                password: data.id,
                email: data.email,
                isActive: true
            }).save();
        });
    }
};
UserService = __decorate([
    typedi_1.Service()
], UserService);
exports.UserService = UserService;
