"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Admin_1 = __importDefault(require("../models/Admin"));
const User_1 = __importDefault(require("../models/User"));
const picture_helper_1 = require("../helpers/picture.helper");
const apollo_server_koa_1 = require("apollo-server-koa");
const mongodb_1 = require("mongodb");
const typedi_1 = require("typedi");
let AdminService = class AdminService {
    // Create an admin
    createAdmin(adminBody) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (yield User_1.default.findOne({ email: adminBody.email })) {
                    let error = new mongodb_1.MongoError("Email ya se encuentra en uso");
                    error.code = 11000;
                    throw error;
                }
                let admin = new Admin_1.default(adminBody);
                admin.picture = picture_helper_1.generatePicture(`${admin.name} ${admin.lastName}`);
                yield admin.save();
                return {
                    message: "Administrador creado",
                    isSuccess: true
                };
            }
            catch (error) {
                console.log(error);
                if (error.code == 11000) {
                    return {
                        message: "Email ya se encuentra en uso",
                        isSuccess: false
                    };
                }
                else {
                    throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
                }
            }
        });
    }
    // Login
    adminLogin(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let admin = yield Admin_1.default.findOne({ email });
                if (admin) {
                    if (admin.comparePassword(password)) {
                        return {
                            isSuccess: true,
                            message: admin.generateToken()
                        };
                    }
                    else {
                        return {
                            isSuccess: false,
                            message: "Contraseña invalida"
                        };
                    }
                }
                else {
                    return {
                        isSuccess: false,
                        message: "No se encontro un usuario con este correo"
                    };
                }
            }
            catch (error) {
                return {
                    isSuccess: false,
                    message: "Ocurrio un error"
                };
            }
        });
    }
    // Update an admin
    updateAdmin(adminBody, id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Admin_1.default.findByIdAndUpdate(id, adminBody);
                return {
                    message: "Usuario actualizado",
                    isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Get an admin by id (Id is provided by a token)
    findAdminById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield Admin_1.default.findById(id);
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Delete an admin by id (Id is provided by a token)
    deleteAdmin(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield Admin_1.default.findByIdAndRemove(id);
                return {
                    message: "Cuenta de usuario eliminada", isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
};
AdminService = __decorate([
    typedi_1.Service()
], AdminService);
exports.AdminService = AdminService;
