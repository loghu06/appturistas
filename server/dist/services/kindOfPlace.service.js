"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const KindOfPlace_1 = __importDefault(require("../models/KindOfPlace"));
const apollo_server_koa_1 = require("apollo-server-koa");
let kindOfPlaceService = class kindOfPlaceService {
    // Get all kind of places
    getKindOfPlaces() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield KindOfPlace_1.default.find();
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Create a kind of place
    createKind(kind) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield new KindOfPlace_1.default(kind).save();
                return {
                    message: "Tipo de lugar creado",
                    isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
    // Update a kind of place
    updateKind(kind) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield KindOfPlace_1.default.findByIdAndUpdate(kind._id, kind);
                return {
                    message: "Tipo de lugar actualizado",
                    isSuccess: true
                };
            }
            catch (error) {
                throw new apollo_server_koa_1.ApolloError("Ocurrio un error");
            }
        });
    }
};
kindOfPlaceService = __decorate([
    typedi_1.Service()
], kindOfPlaceService);
exports.kindOfPlaceService = kindOfPlaceService;
