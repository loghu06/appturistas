"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Authentication middleware. Looks for a valid token
exports.isAuth = ({ root, args, context, info }, roles) => {
    let isValidUser = false;
    roles.forEach(role => {
        if (context.auth[role]) {
            isValidUser = true;
        }
    });
    return isValidUser;
};
