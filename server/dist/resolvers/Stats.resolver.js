"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const Stats_type_1 = require("../types/Stats.type");
const stats_service_1 = require("../services/stats.service");
let StatsResolver = class StatsResolver {
    constructor(statsResolver) {
        this.statsResolver = statsResolver;
    }
    getStats() {
        return this.statsResolver.getStats();
    }
};
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Query(returns => [Stats_type_1.Stats], { name: "Stats" }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], StatsResolver.prototype, "getStats", null);
StatsResolver = __decorate([
    type_graphql_1.Resolver(Stats_type_1.Stats),
    __metadata("design:paramtypes", [stats_service_1.StatsService])
], StatsResolver);
exports.StatsResolver = StatsResolver;
