"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const Place_type_1 = require("../types/Place.type");
const Movement_type_1 = require("../types/Movement.type");
const place_service_1 = require("../services/place.service");
let PlaceResolver = class PlaceResolver {
    constructor(placeService) {
        this.placeService = placeService;
    }
    // Get a specific place by id
    getPlace(id) {
        return this.placeService.getPlace(id);
    }
    // Get all places
    getPlaces() {
        return this.placeService.getPlaces();
    }
    // Create a place
    createPlace(placeBody) {
        return this.placeService.createPlace(placeBody);
    }
    // Update a place
    updatePlace(placeBody) {
        return this.placeService.updatePlace(placeBody);
    }
    // Delete a place
    deletePlace(id) {
        return this.placeService.deletePlace(id);
    }
};
__decorate([
    type_graphql_1.Authorized("admin", "user"),
    type_graphql_1.Query(returns => Place_type_1.Place, { name: "Place" }),
    __param(0, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PlaceResolver.prototype, "getPlace", null);
__decorate([
    type_graphql_1.Authorized("admin", "user"),
    type_graphql_1.Query(returns => [Place_type_1.Place], { name: "Places" }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PlaceResolver.prototype, "getPlaces", null);
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "Place" }),
    __param(0, type_graphql_1.Arg("place")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Place_type_1.PlaceInput]),
    __metadata("design:returntype", void 0)
], PlaceResolver.prototype, "createPlace", null);
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "UpdatePlace" }),
    __param(0, type_graphql_1.Arg("place")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Place_type_1.PlaceInput]),
    __metadata("design:returntype", void 0)
], PlaceResolver.prototype, "updatePlace", null);
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "DeletePlace" }),
    __param(0, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PlaceResolver.prototype, "deletePlace", null);
PlaceResolver = __decorate([
    type_graphql_1.Resolver(Place_type_1.Place),
    __metadata("design:paramtypes", [place_service_1.PlaceService])
], PlaceResolver);
exports.PlaceResolver = PlaceResolver;
