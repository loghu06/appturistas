"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const Review_type_1 = require("../types/Review.type");
const Movement_type_1 = require("../types/Movement.type");
const review_service_1 = require("../services/review.service");
let ReviewResolver = class ReviewResolver {
    constructor(reviewService) {
        this.reviewService = reviewService;
    }
    getReviews(placeID) {
        return this.reviewService.getReviews(placeID);
    }
    getUserReviews({ auth }) {
        return this.reviewService.getUserReviews(auth.user);
    }
    getReview({ auth }, placeID) {
        return this.reviewService.getReview(placeID, auth.user);
    }
    createReview(ctx, reviewBody) {
        reviewBody.user = ctx.auth.user;
        return this.reviewService.createReview(reviewBody);
    }
    updateReview(ctx, reviewBody) {
        return this.reviewService.updateReview(reviewBody, ctx.auth.user);
    }
    deleteReview(ctx, reviewID) {
        return this.reviewService.deleteReview(reviewID, ctx.auth.user);
    }
};
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Query(returns => [Review_type_1.Review], { name: "PlaceReviews" }),
    __param(0, type_graphql_1.Arg("place")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ReviewResolver.prototype, "getReviews", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Query(returns => [Review_type_1.Review], { name: "UserReviews" }),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], ReviewResolver.prototype, "getUserReviews", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Query(returns => Review_type_1.Review, { name: "Review" }),
    __param(0, type_graphql_1.Ctx()),
    __param(1, type_graphql_1.Arg("place")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", void 0)
], ReviewResolver.prototype, "getReview", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "Review" }),
    __param(0, type_graphql_1.Ctx()),
    __param(1, type_graphql_1.Arg("review")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Review_type_1.ReviewInput]),
    __metadata("design:returntype", void 0)
], ReviewResolver.prototype, "createReview", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "UpdateReview" }),
    __param(0, type_graphql_1.Ctx()),
    __param(1, type_graphql_1.Arg("review")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Review_type_1.ReviewInput]),
    __metadata("design:returntype", void 0)
], ReviewResolver.prototype, "updateReview", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "DeleteReview" }),
    __param(0, type_graphql_1.Ctx()),
    __param(1, type_graphql_1.Arg("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", void 0)
], ReviewResolver.prototype, "deleteReview", null);
ReviewResolver = __decorate([
    type_graphql_1.Resolver(Review_type_1.Review),
    __metadata("design:paramtypes", [review_service_1.ReviewService])
], ReviewResolver);
exports.ReviewResolver = ReviewResolver;
