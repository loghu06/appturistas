"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const KindOfPlace_type_1 = require("../types/KindOfPlace.type");
const Movement_type_1 = require("../types/Movement.type");
const kindOfPlace_service_1 = require("../services/kindOfPlace.service");
let KindOfPlaceResolver = class KindOfPlaceResolver {
    constructor(kindService) {
        this.kindService = kindService;
    }
    // Kind of places available
    getKindOfPlaces() {
        return this.kindService.getKindOfPlaces();
    }
    // Create a new kind of place
    createKind(kindBody) {
        return this.kindService.createKind(kindBody);
    }
    // Update a kind of place
    updateKind(kindBody) {
        return this.updateKind(kindBody);
    }
};
__decorate([
    type_graphql_1.Authorized("admin", "user"),
    type_graphql_1.Query(returns => [KindOfPlace_type_1.KindOfPlace], { name: "Kinds" }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], KindOfPlaceResolver.prototype, "getKindOfPlaces", null);
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "Kind" }),
    __param(0, type_graphql_1.Arg("kind")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KindOfPlace_type_1.KindOfPlaceInput]),
    __metadata("design:returntype", void 0)
], KindOfPlaceResolver.prototype, "createKind", null);
__decorate([
    type_graphql_1.Authorized("admin"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "UpdateKind" }),
    __param(0, type_graphql_1.Arg("kind")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [KindOfPlace_type_1.KindOfPlaceInput]),
    __metadata("design:returntype", void 0)
], KindOfPlaceResolver.prototype, "updateKind", null);
KindOfPlaceResolver = __decorate([
    type_graphql_1.Resolver(KindOfPlace_type_1.KindOfPlace),
    __metadata("design:paramtypes", [kindOfPlace_service_1.kindOfPlaceService])
], KindOfPlaceResolver);
exports.KindOfPlaceResolver = KindOfPlaceResolver;
