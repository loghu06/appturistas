"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const User_type_1 = require("../types/User.type");
const Movement_type_1 = require("../types/Movement.type");
const user_service_1 = require("../services/user.service");
let UserResolver = class UserResolver {
    constructor(userService) {
        this.userService = userService;
    }
    // User login with email and password
    login(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.login(email, password);
        });
    }
    // Sign in via Facebook
    facebookSignIn(token) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.facebookSignIn(token);
        });
    }
    // Sign up via facebook
    facebookSignUp(token) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.facebookSignUp(token);
        });
    }
    // Send a verification email
    resendActivation(ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.userService.sendActivation(ctx.auth.user);
        });
    }
    // Get a user (should have a valid token)
    getUser(ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.findUserById(ctx.auth.user);
        });
    }
    // Create a user
    createUser(userBody) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.createUser(userBody);
        });
    }
    // Update a user
    updateUser(userBody, ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.updateUser(userBody, ctx.auth.user);
        });
    }
    // Delete a user (Should have a token to ensure the user is deleting his account)
    deleteUser(ctx) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.userService.deleteUser(ctx.auth.user);
        });
    }
};
__decorate([
    type_graphql_1.Query(returns => Movement_type_1.Movement, { name: "Login" }),
    __param(0, type_graphql_1.Arg("email")),
    __param(1, type_graphql_1.Arg("password")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "login", null);
__decorate([
    type_graphql_1.Query(returns => Movement_type_1.Movement, { name: "FacebookSignIn" }),
    __param(0, type_graphql_1.Arg("token")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "facebookSignIn", null);
__decorate([
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "FacebookSignUp" }),
    __param(0, type_graphql_1.Arg("token")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "facebookSignUp", null);
__decorate([
    type_graphql_1.Query(returns => Movement_type_1.Movement, { name: "ResendActivation" }),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "resendActivation", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Query(returns => User_type_1.User, { name: "User" }),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "getUser", null);
__decorate([
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "User" }),
    __param(0, type_graphql_1.Arg("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_type_1.UserInput]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "createUser", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "UpdateUser" }),
    __param(0, type_graphql_1.Arg("user")),
    __param(1, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_type_1.UserInput, Object]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "updateUser", null);
__decorate([
    type_graphql_1.Authorized("user"),
    type_graphql_1.Mutation(returns => Movement_type_1.Movement, { name: "DeleteUser" }),
    __param(0, type_graphql_1.Ctx()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "deleteUser", null);
UserResolver = __decorate([
    type_graphql_1.Resolver(User_type_1.User),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserResolver);
exports.UserResolver = UserResolver;
