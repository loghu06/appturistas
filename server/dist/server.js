"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const type_graphql_1 = require("type-graphql");
const apollo_server_koa_1 = require("apollo-server-koa");
const koa_1 = __importDefault(require("koa"));
const auth_1 = require("./helpers/auth");
// Authentication middleware
const authentication_1 = require("./middlewares/authentication");
// Dependency injection
const typedi_1 = require("typedi");
class Server {
    constructor(PORT) {
        this.PORT = PORT;
        this.koa = new koa_1.default(); // Koa server
        this.setUpServer();
    }
    setUpServer() {
        return __awaiter(this, void 0, void 0, function* () {
            this.graphQLServer = new apollo_server_koa_1.ApolloServer({
                schema: yield type_graphql_1.buildSchema({
                    resolvers: [__dirname + "/resolvers/*.resolver.*s"],
                    authChecker: authentication_1.isAuth,
                    container: typedi_1.Container // Dependency injection container
                }),
                subscriptions: {
                    onConnect: (params, webSocket) => {
                        if (params.token) { // If has a token then verify it's a valid one
                            return { auth: auth_1.decodeToken(params.token) };
                        }
                        else { // If it's an invalid token don't let the user to connect
                            throw new apollo_server_koa_1.ApolloError("Need a valid token");
                        }
                    }
                },
                context: ({ connection, ctx }) => __awaiter(this, void 0, void 0, function* () {
                    if (connection) {
                        return connection.context;
                    }
                    else { // On every connection returns the verified token
                        return { auth: auth_1.decodeToken(ctx.request.header.authorization) };
                    }
                })
            });
            // Once the schema is ready then run the server in the given port.
            this.runServer();
        });
    }
    runServer() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let subscriptionsServer = yield this.koa.listen(this.PORT);
                this.graphQLServer.applyMiddleware({ app: this.koa });
                this.graphQLServer.installSubscriptionHandlers(subscriptionsServer);
                console.log(`Listening on http://localhost:${this.PORT}`);
            }
            catch (error) {
                console.log(error);
            }
        });
    }
}
exports.Server = Server;
