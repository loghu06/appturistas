import { Resolver, Mutation, Query, Arg, Authorized, Ctx } from "type-graphql";
import { Review, ReviewInput } from "../types/Review.type";
import { Movement } from "../types/Movement.type";
import { ReviewService } from "../services/review.service";

@Resolver(Review)
export class ReviewResolver {
    constructor(private reviewService: ReviewService){}
    
    @Authorized("user")
    @Query(returns => [Review], {name: "PlaceReviews"})
    public getReviews(@Arg("place") placeID: string){
        return this.reviewService.getReviews(placeID)
    }

    @Authorized("user")
    @Query(returns => [Review], {name: "UserReviews"})
    public getUserReviews(@Ctx() {auth}: any){
        return this.reviewService.getUserReviews(auth.user)
    }
    
    @Authorized("user")
    @Query(returns => Review, {name: "Review"})
    public getReview(
        @Ctx() {auth}: any,
        @Arg("place") placeID: string
    ){
        return this.reviewService.getReview(placeID, auth.user)
    }
    
    @Authorized("user")
    @Mutation(returns => Movement, {name: "Review"})
    public createReview(
        @Ctx() ctx: any,
        @Arg("review") reviewBody: ReviewInput
    ){
        reviewBody.user = ctx.auth.user
        return this.reviewService.createReview(reviewBody)
    }

    @Authorized("user")
    @Mutation(returns => Movement, {name: "UpdateReview"})
    public updateReview(
        @Ctx() ctx: any,
        @Arg("review") reviewBody: ReviewInput
    ){
        return this.reviewService.updateReview(reviewBody, ctx.auth.user)
    }

    @Authorized("user")
    @Mutation(returns => Movement, {name: "DeleteReview"})
    public deleteReview(
        @Ctx() ctx: any,
        @Arg("id") reviewID: string
    ){
        return this.reviewService.deleteReview(reviewID, ctx.auth.user)
    }
}