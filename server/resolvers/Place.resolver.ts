import { Resolver, Mutation, Query, Arg, Ctx, Authorized } from 'type-graphql'
import { Place, PlaceInput } from '../types/Place.type'
import { Movement } from '../types/Movement.type';
import { PlaceService } from '../services/place.service';

@Resolver(Place)
export class PlaceResolver {
    
    constructor(private placeService: PlaceService){}
    // Get a specific place by id
    @Authorized("admin", "user")
    @Query(returns => Place, {name: "Place"})
    public getPlace(@Arg("id") id: string){
        return this.placeService.getPlace(id)
    }
    // Get all places
    @Authorized("admin", "user")
    @Query(returns => [Place], {name: "Places"})
    public getPlaces(){
        return this.placeService.getPlaces()
    }
    // Create a place
    @Authorized("admin")
    @Mutation(returns => Movement, {name: "Place"})
    public createPlace(@Arg("place") placeBody: PlaceInput){
        return this.placeService.createPlace(placeBody)
    }
    // Update a place
    @Authorized("admin")
    @Mutation(returns => Movement, {name: "UpdatePlace"})
    public updatePlace(@Arg("place") placeBody: PlaceInput){
        return this.placeService.updatePlace(placeBody)
    }
    // Delete a place
    @Authorized("admin")
    @Mutation(returns => Movement, {name: "DeletePlace"})
    public deletePlace(@Arg("id") id: string){
        return this.placeService.deletePlace(id)
    }
}