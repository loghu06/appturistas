import { Resolver, Mutation, Query, Authorized, Arg } from 'type-graphql'
import { KindOfPlace, KindOfPlaceInput } from '../types/KindOfPlace.type';
import { Movement } from '../types/Movement.type'
import { kindOfPlaceService } from '../services/kindOfPlace.service';

@Resolver(KindOfPlace)
export class KindOfPlaceResolver {

    constructor(private kindService: kindOfPlaceService){}    
    // Kind of places available
    @Authorized("admin", "user")
    @Query(returns => [KindOfPlace], {name: "Kinds"})
    public getKindOfPlaces(){
        return this.kindService.getKindOfPlaces()
    }
    // Create a new kind of place
    @Authorized("admin")
    @Mutation(returns => Movement, {name: "Kind"})
    public createKind(@Arg("kind") kindBody: KindOfPlaceInput){
        return this.kindService.createKind(kindBody)
    }
    // Update a kind of place
    @Authorized("admin")
    @Mutation(returns => Movement, {name: "UpdateKind"})
    public updateKind(@Arg("kind") kindBody: KindOfPlaceInput){
        return this.updateKind(kindBody)
    }
}