import { Resolver, Authorized, Query } from "type-graphql";
import { Stats } from "../types/Stats.type";
import { StatsService } from "../services/stats.service";

@Resolver(Stats)
export class StatsResolver {
  constructor(private statsResolver: StatsService){}

  @Authorized("admin")
  @Query(
    returns => [Stats], {name: "Stats"}
  )
  public getStats(){
    return this.statsResolver.getStats()
  }
}