import { Resolver, Query, Mutation, Arg, Ctx, Authorized } from 'type-graphql'
import { User, UserInput } from '../types/User.type';
import { Movement } from '../types/Movement.type';
import { UserService } from '../services/user.service';
import { IsEmail } from 'class-validator';

@Resolver(User)
export class UserResolver {
    constructor(private userService: UserService){}
    // User login with email and password
    @Query(returns => Movement, {name: "Login"})
    async login(
        @Arg("email") email: string,
        @Arg("password") password: string
    ){
        return this.userService.login(email, password)
    }
    // Sign in via Facebook
    @Query(returns => Movement, {name: "FacebookSignIn"})
    async facebookSignIn(
        @Arg("token") token: string
    ){
        return this.userService.facebookSignIn(token)
    }
    // Sign up via facebook
    @Mutation(returns => Movement, {name: "FacebookSignUp"})
    async facebookSignUp(
        @Arg("token") token: string
    ){
        return this.userService.facebookSignUp(token)
    }
    // Send a verification email
    @Query(returns => Movement, {name: "ResendActivation"})
    async resendActivation(@Ctx() ctx: any){
        return await this.userService.sendActivation(ctx.auth.user)
    }
    // Get a user (should have a valid token)
    @Authorized("user")
    @Query(returns => User, {name: "User"})
    async getUser(@Ctx() ctx: any){
        return this.userService.findUserById(ctx.auth.user)
    }
    // Create a user
    @Mutation(returns => Movement, {name: "User"})
    async createUser(@Arg("user") userBody: UserInput){
        return this.userService.createUser(userBody)
    }
    // Update a user
    @Authorized("user")
    @Mutation(returns => Movement, {name: "UpdateUser"})
    async updateUser(
        @Arg("user") userBody: UserInput,
        @Ctx() ctx: any
    ){
        return this.userService.updateUser(userBody, ctx.auth.user)
    }
    // Delete a user (Should have a token to ensure the user is deleting his account)
    @Authorized("user")
    @Mutation(returns => Movement, {name: "DeleteUser"})
    async deleteUser(@Ctx() ctx: any){
        return this.userService.deleteUser(ctx.auth.user)
    }
}