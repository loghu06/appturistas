import { Resolver, Query, Mutation, Arg, Authorized, Ctx } from 'type-graphql'
import { Admin, AdminInput } from '../types/Admin.type'
import { Movement } from '../types/Movement.type';
import { AdminService } from '../services/admin.service';

@Resolver(Admin)
export class AdminResolver {
    
    constructor(private adminService: AdminService){}
    // Get admin data
    @Authorized("admin")
    @Query(returns => Admin, {name: "Admin"})
    public async getAdmin(@Ctx() ctx: any){
        return this.adminService.findAdminById(ctx.auth.admin)
    }

    @Query(returns => Movement, {name: "AdminLogin"})
    public async adminLogin(
        @Arg("email") email: string,
        @Arg("password") password: string
    ){
        return this.adminService.adminLogin(email, password)
    }
    
    // New admin
    @Mutation(returns => Movement, {name: "Admin"})
    public async createAdmin(
        @Arg("admin") adminBody: AdminInput
    ){
        return this.adminService.createAdmin(adminBody)
    }

    // Update admin
    @Authorized("admin")
    @Mutation(returns => Movement, {name: "UpdateAdmin"})
    public async updateAdmin(
        @Ctx() ctx: any,
        @Arg("admin") adminBody: AdminInput
    ){
        return this.adminService.updateAdmin(adminBody, ctx.auth.admin)
    }

    @Authorized("admin")
    @Mutation(returns => Movement, {name: "DeleteAdmin"})
    public async deleteAdmin(@Ctx() ctx: any){
        return this.adminService.deleteAdmin(ctx.auth.admin)
    }
}