import { Typegoose, prop } from 'typegoose'

export class KindOfPlace extends Typegoose {
    @prop({ 
        required: true, unique: true,
        enum: ["Food", "Park", "Museum", "Shopping", "Hotel", "Bar", "Coffee", "Church"]
    })
    kind: string
}

export default new KindOfPlace().getModelForClass(KindOfPlace)