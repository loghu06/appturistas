import { prop, instanceMethod, InstanceType } from 'typegoose'
import { User } from './User'
import mongoose from 'mongoose'
import { sign } from 'jsonwebtoken'
import { SECRET_KEY } from '../config';

class Admin extends User {
    @prop({ default: true, required: true })
    isAdmin: boolean

    @instanceMethod
    generateToken(this: InstanceType<Admin>): string {
        return sign({
            admin: this._id
        }, SECRET_KEY, {
            expiresIn: '1y'
        })
    }
}

export default new Admin().setModelForClass(Admin, {
    existingMongoose: mongoose,
    schemaOptions: { collection: 'admins' }
})