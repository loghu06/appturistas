import { Typegoose, prop, Ref } from 'typegoose'
import { KindOfPlace } from './KindOfPlace'

export class Place extends Typegoose {
    @prop({ required: true })
    name: string
    @prop({ required: true })
    latitude: number
    @prop({ required: true })
    longitude: number
    @prop({ required: true })
    kindOfPlace: Ref<KindOfPlace>
    @prop({ required: false, default: []})
    pictures: Array<string>
}

export default new Place().getModelForClass(Place)