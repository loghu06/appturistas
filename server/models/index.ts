import mongoose, { ConnectionOptions } from 'mongoose'

// Parse _id to String
/* Types.ObjectId.prototype.valueOf = function(){
    return this.toString()
} */

export class Mongoose {    
    constructor(
        private mongoUri: string, // Url
        private mongoOptions: ConnectionOptions // user / pass options
    ){
        this.setUpDatabase() // Start mongo config
    }

    private async setUpDatabase(){
        try { // Try to make a connection
            await mongoose.connect(this.mongoUri, this.mongoOptions)
            console.log("Mongo is ready")
        } catch (error) {
            console.log(error)
        }
    }
}