import { Typegoose, prop, Ref } from 'typegoose'
import { User } from './User'
import { Place } from './Place'

class Review extends Typegoose {
    @prop({ required: true })
    user: Ref<User>
    @prop({ required: true })
    place: Ref<Place>
    @prop({ required: true })
    message: string
    @prop({ required: true })
    date: string
    @prop({ required: true })
    rating: number
}

export default new Review().getModelForClass(Review)