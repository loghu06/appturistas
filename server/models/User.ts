import { Typegoose, prop, pre, instanceMethod, InstanceType, Ref, arrayProp } from 'typegoose'
import { compareSync, hashSync } from 'bcrypt'
import { sign } from 'jsonwebtoken'
import { ROUNDS, SECRET_KEY } from '../config';
import { KindOfPlace } from './KindOfPlace'

@pre<User>('findOneAndUpdate', function(){ // Before an update ensure to create a hash for the password
    if(this._update.password){
        this._update.password = hashSync(this._update.password, ROUNDS)
    }
})
@pre<User>('save', function(next){ // Before a 'save' ensure to create a hash for the password
    if(this.isModified('password')){
        this.password = hashSync(this.password, ROUNDS)
    }
    next()
})
export class User extends Typegoose {
    @prop({ required: true }) // User's name
    name: string
    @prop({ required: true }) // User's lastname
    lastName: string
    @prop({ required: true, unique: true }) // Valid email
    email: string
    @prop({ required: true }) // User's password
    password: string
    @prop({ required: false, unique: false }) // Facebook's id for login
    facebookID: string
    @prop({ required: true }) // Some picture
    picture: string
    @prop({ required: true })
    birthDate: string // Birth Date
    @prop({ required: true })
    state: string // State where the person lives
    @arrayProp({itemsRef: 'KindOfPlace', default: []}) // Favorite places
    favoritePlaces: Ref<KindOfPlace>[]
    @prop({ required: true, default: false }) // Is not active until the email is verified
    isActive: boolean

    @instanceMethod
    comparePassword(this: InstanceType<User>, password: string): boolean {
        return compareSync(password, this.password)
    }

    @instanceMethod // Create a token
    generateToken(this: InstanceType<User>): string {
        return sign({
            user: this._id
        }, SECRET_KEY, {
            expiresIn: '1y'
        })
    }

    @instanceMethod
    activationLink(this: InstanceType<User>){
        // Activation link, valid for 30 minutes
        return sign({
            _id: this._id
        }, SECRET_KEY,
        {
            expiresIn: "30m"
        })
    }
}

export default new User().getModelForClass(User)