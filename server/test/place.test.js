const expect = require('chai').expect
const url = "http://localhost:7000/graphql"
const request = require('supertest')(url)

describe("Place test", () => {
    let testPlace = ""
    let adminToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6IjVkNGRjOTNkZDg4NTMyMjI0NGVlZWY2MSIsImlhdCI6MTU2NjY4NzQ1MywiZXhwIjoxNTk4MjQ1MDUzfQ.PONlxc27t2BhzgwmGPeN7Nidt-x0dus3v7Xu9wcCo6U"

    it("Create place", async () => {
        try {
            var {body} = await request.post("")
            .set("token", adminToken)
            .send({
                query: `
                    mutation {
                        Place(
                            place: {
                                name: "test",
                                longitude: 20.2896124,
                                latitude: -99.21742
                            }
                        ){ message isSuccess }
                    }
                `
            })
        } catch (error) {}
        testPlace = body.data.Place.message
        expect(body.data.Place.isSuccess).to.equal(true)
    })

    it("Update place", async () => {
        try {
            var {body} = await request.post("")
            .set("token", adminToken)
            .send({
                query: `
                    mutation {
                        UpdatePlace(
                            place: {
                                _id: "${testPlace}"
                                name: "Updated test name"
                            }
                        ){ isSuccess }
                    }
                `
            })
        } catch (error) {}

        expect(body.data.UpdatePlace.isSuccess).to.equal(true)
    })

    it("Get place data", async () => {
        try {
            var {body} = await request.post("")
            .set("token", adminToken)
            .send({
                query: `{ Place(id: "${testPlace}"){ _id name } }`
            })
        } catch (error) {}
        console.log(body.data.Place)
        expect(body.data.Place).to.have.property("_id")
    })

    it("Get all places", async () => {
        try {
            var {body} = await request.post("")
            .set("token", adminToken)
            .send({
                query: `{ Places { name } }`
            })
        } catch (error) {}
        console.log(body.data.Places)
        expect(body.data.Places.length).to.be.above(0)
    })

    it("Delete place", async () => {
        try {
            var {body} = await request.post("")
            .set("token", adminToken)
            .send({
                query: `
                    mutation {
                        DeletePlace(id: "${testPlace}"){ isSuccess }
                    }
                `
            })
        } catch (error) {}

        expect(body.data.DeletePlace.isSuccess).to.equal(true)
    })
})