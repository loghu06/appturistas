const chai = require('chai')
const expect = chai.expect
const url = "http://localhost:9008"
const request = require('supertest')(url)

describe('Hello World test', () => {
    it("Returns a Hello World :D", async () => {
        try {
            var {body} = await request.post('/graphql').send({ query: "{ Hello }" })
        } catch (error) {}

        expect(body.data).to.have.property("Hello")
    })
})