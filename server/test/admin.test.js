const expect = require('chai').expect
const url = "http://localhost:7000/graphql"
const request = require('supertest')(url)

describe("Admin test", () => {
    let testToken = ""
    // let testEmail = "15690218@tecvalles.mx"
    let testEmail = "admin@admin.com"
    let testPassword = "test00"

    it("Create a new admin", async () => {
        try {
            var {body} = await request.post("")
            .send({
                query: `
                    mutation {
                        Admin(
                            admin: {
                                name: "test",
                                lastName: "test",
                                email: "${testEmail}",
                                password: "${testPassword}"
                            }
                        ){
                            message isSuccess
                        }
                    }
                `
            })
        } catch (error) {}
        console.log(body.data)
        expect(body.data.Admin.isSuccess).to.equal(true)
    })

    it("Admin login", async () => {
        try {
            var {body} = await request.post("")
            .send({
                query: `
                    {
                        Login(
                            email: "${testEmail}",
                            password: "${testPassword}"
                        ){ message isSuccess }
                    }
                `
            })
        } catch (error) {}
        testToken = body.data.Login.message
        expect(body.data.Login.isSuccess).to.equal(true)
    })

    it("Update admin", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({
                query: `
                    mutation {
                        UpdateAdmin(
                            admin: {
                                name: "Test name"
                            }
                        ){ message isSuccess }
                    }
                `
            })
        } catch (error) {}

        expect(body.data.UpdateAdmin.isSuccess).to.equal(true)
    })
    
    it("Get admin data", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({
                query: `
                    { Admin { _id name } }
                `
            })
        } catch (error) {}
        
        console.log(body.data.Admin)
        expect(body.data.Admin).to.have.property("_id")
    })

    it("Delete admin", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({
                query: ` mutation { DeleteAdmin { isSuccess } } `
            })
        } catch (error) {}

        expect(body.data.DeleteAdmin.isSuccess).to.equal(true)
    })
})