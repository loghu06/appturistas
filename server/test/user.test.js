const expect = require('chai').expect
const url = "http://localhost:7000/graphql"
const request = require('supertest')(url)

describe('User test', () => {
    let testToken = ""
    let testEmail = "test@test.com"
    let testPassword = "test00"

    it("Create a new user", async () => {
        try {
            var {body} = await request.post("")
            .send({
                query: `
                mutation { 
                    User(
                        user: {
                            name: "test", 
                            lastName: "test", 
                            email: "${testEmail}", 
                            password: "${testPassword}"
                        }
                    ){ message isSuccess } 
                }`
            })
        } catch (error) {}

        expect(body.data.User.isSuccess).to.equal(true)
    })
    
    it("Successful login test", async () => {
        try {
            var {body} = await request.post('')
            .send({
                query: `{ Login(email: "${testEmail}", password: "${testPassword}"){message isSuccess} }`
            })
        } catch (error) {}
        testToken = body.data.Login.message
        // If returns a token should be true
        expect(body.data.Login.isSuccess).to.equal(true)
    })

    it("Invalid password", async () => {
        try {
            var {body} = await request.post('')
            .send({ query: `{ Login(email: "${testEmail}", password: "invalidPassword"){message isSuccess} }` })
        } catch (error) {}
        // console.log(body.data.Login.message)
        expect(body.data.Login.isSuccess).to.equal(false)
    })

    it("Invalid email", async () => {
        try {
            var {body} = await request.post('')
            .send({ query: `{ Login(email: "invalid@email.com", password: "invalidPassword"){message isSuccess} }` })
        } catch (error) {}
        /* console.log(body.data.Login.message) */
        expect(body.data.Login.isSuccess).to.equal(false)
    })

    it("Update user", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({
                query: `
                    mutation {
                        UpdateUser(
                            user: {
                                name: "Update test"
                            }
                        ){
                            isSuccess
                        }
                    }
                `
            })
        } catch (error) { }

        expect(body.data.UpdateUser.isSuccess).to.equal(true)
    })

    it("Getting user data", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({ 
                query: `{ User { _id name } }`
            })
        } catch (error) {}

        console.log(body.data.User)
        expect(body.data.User).to.have.property('_id')
    })

    /* it("Resend activation link", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({
                query: `
                    { ResendActivation { message isSuccess } }
                `
            })
        } catch (error) {}

        expect(body.data.ResendActivation.isSuccess).to.equal(true)
    }) */

    it("Remove test account", async () => {
        try {
            var {body} = await request.post("")
            .set("token", testToken)
            .send({ 
                query: `
                mutation {
                    DeleteUser { message isSuccess }
                }`

            })
        } catch (error) {}
        
        console.log(body.data)
        expect(body.data.DeleteUser.isSuccess).to.equal(true)
    })
})