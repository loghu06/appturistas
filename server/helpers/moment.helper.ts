import moment from 'moment'

moment.locale("es", {
    months: [
        "Enero", "Febrero", "Marzo", "Abril",
        "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ],
    monthsShort: [
        "En", "Feb", "Mar", "Abr",
        "May", "Jun", "Jul", "Ag",
        "Sep", "Oct", "Nov", "Dic"
    ],
    weekdays: [
        "Domingo", "Lunes", "Martes", "Miercoles",
        "Jueves", "Viernes", "Sabado"
    ],
    weekdaysMin: [
        "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"
    ],
    weekdaysShort: [
        "Dom", "Lun", "Mar", "Mi", "Ju", "Vi", "Sa"
    ]
})

export default moment