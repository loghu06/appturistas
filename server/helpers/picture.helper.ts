import { generateAvatar } from 'ui-avatars'

export function generatePicture(name: string): string {
    return generateAvatar({
        name,
        uppercase: true,
        background: "000000",
        color: "FFFFFF",
        fontsize: 0.5,
        bold: true,
        length: 2,
        rounded: true,
        size: 250
    })
}