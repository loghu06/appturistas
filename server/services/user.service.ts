import axios from 'axios'
import AdminModel from '../models/Admin'
import UserModel from '../models/User' // User model
import { User } from '../types/User.type'
import { Movement } from '../types/Movement.type'
import { generatePicture } from '../helpers/picture.helper';
import { ApolloError } from 'apollo-server-koa';
import { mailer } from '../helpers/email.helper';
import { Service } from 'typedi'

interface FacebookResponse {
    data: {
        id: string
        first_name: string
        last_name: string
        email: string
        picture: {
            data: {
                width: number
                height: number
                is_silhouette: boolean
                url: string
            }
        }
    }
}

@Service()
export class UserService {
    private facebookUrl: string = "https://graph.facebook.com/v2.12/me?fields=id,first_name,last_name,email,picture.type(large)&access_token=";
    // Create a user
    public async createUser(userBody: User): Promise<Movement>{
        try {
            let user = new UserModel(userBody)
            user.picture = generatePicture(`${user.name} ${user.lastName}`)
            await user.save()

            return {
                message: "Usuario registrado",
                isSuccess: true,
                token: user.generateToken()
            }
        } catch (error) {
            console.log(error)
            if(error.code == 11000){
                return {
                    message: "Email ya esta en uso",
                    isSuccess: false
                }
            }
            else {
                return {
                    message: "Ocurrio un error",
                    isSuccess: false
                }
            }
        }
    }
    // Sign in handler
    public async login(email: string, password: string): Promise<Movement> {
        try {
            let user = await UserModel.findOne({email})
    
            if(user){
                if(user.comparePassword(password)){
                    return {
                        message: user.generateToken(),
                        isSuccess: true
                    }
                }
                else {
                    return {
                        message: "Contraseña invalida",
                        isSuccess: false
                    }
                }
            }
            else {
                return {
                    message: "Email invalido",
                    isSuccess: false
                }
            }
        } catch (error) {
            return { message: "Ocurrio un error", isSuccess: false }
        }
    }
    // Sign in via Facebook
    public async facebookSignIn(token: string): Promise<Movement> {
        try {
            const data: FacebookResponse = await axios({
                method: "GET",
                url: this.facebookUrl + token
            })
            // Find a user by facebook's id
            const user = await UserModel.findOne({facebookID: data.data.id})
            
            if(user){
                return {
                    message: user.generateToken(),
                    isSuccess: true
                }
            }
            else {
                return {
                    message: "Usuario no se encuentra registrado",
                    isSuccess: false
                }
            }
            
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Sign up via facebook
    public async facebookSignUp(token: string): Promise<Movement> {
        try {
            const data: FacebookResponse = await axios({
                method: "GET",
                url: this.facebookUrl + token
            })
            
            const user: any = await this.createViaFacebookAccount(data)
            
            return {
                message: user.generateToken(),
                isSuccess: true
            }
        } catch (error) {
            if(error.code == 11000){
                console.log(error)
                return {
                    message: "Usuario ya se encuentra registrado",
                    isSuccess: false
                }
            }
            else {
                throw new ApolloError("Ocurrio un error");
            }
        }
    }
    // Get a user by id (Id is provided by a token)
    public async findUserById(id: string){
        try {
            return await UserModel.findById(id).populate({
                path: "favoritePlaces",
                model: "KindOfPlace"
            })
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Update a user
    public async updateUser(userBody: User, userID: string): Promise<Movement>{
        try {
            await UserModel.findByIdAndUpdate(userID, userBody)

            return {
                message: "Usuario actualizado",
                isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Delete a user
    public async deleteUser(userID: string): Promise<Movement> {
        try {
            await UserModel.findByIdAndRemove(userID)
            
            return {
                message: "Usuario eliminado",
                isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }

    // Send activation token to user's email
    public async sendActivation(userID: string): Promise<Movement>{
        try {
            const user = await UserModel.findById(userID)
            const token: string = user.activationLink()

            mailer.sendMail({
                to: user.email,
                subject: "Confirmacion de la cuenta",
                html: `<a href="http://localhost:9008/activate-account/${token}">Click para activar cuenta!</a>`
            })
            
            return {
                message: "Correo de activacion fue enviado",
                isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }

    private async createViaFacebookAccount({data}: FacebookResponse): Promise<any>{
        const picture: string = generatePicture(`${data.first_name} ${data.last_name}`)
        
        return await new UserModel({
            facebookID: data.id,
            name: data.first_name,
            lastName: data.last_name,
            picture,
            password: data.id,
            email: data.email,
            isActive: true
        }).save()
    }
}