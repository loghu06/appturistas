import PlaceModel from '../models/Place'
import { Place } from '../types/Place.type'
import { ApolloError } from 'apollo-server-koa';
import { Movement } from '../types/Movement.type';
import { Service } from 'typedi';

@Service()
export class PlaceService {
    // Get a place by id
    public async getPlace(id: string): Promise<any> {
        try { 
            return await PlaceModel.findById(id)
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Get all places
    public async getPlaces(): Promise<any[]> {
        try {
            return await PlaceModel.find().populate({
                path: "kindOfPlace",
                model: "KindOfPlace"
            })
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Create a place
    public async createPlace(placeBody: Place): Promise<Movement> {
        try {
            const place = await new PlaceModel(placeBody).save()
            
            return {
                message: "Lugar creado con exito", isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Update a place
    public async updatePlace(placeBody: Place): Promise<Movement> {
        try {
            await PlaceModel.findByIdAndUpdate(placeBody._id, placeBody)
            
            return {
                message: "Lugar actualizado", isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Delete a place
    public async deletePlace(id: string): Promise<Movement> {
        try {
            await PlaceModel.findByIdAndRemove(id)

            return {
                message: "Lugar eliminado", isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
}