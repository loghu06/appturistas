import { Service } from "typedi";
import { ApolloError } from "apollo-server-koa";
import ReviewModel from '../models/Review'
import moment = require("moment");

@Service()
export class StatsService {
  public async getStats(){
    try {
      const currentData: moment.Moment = moment()
      // Menor de 18
      const lessThan18: number = currentData.subtract(18,"years").year() 
      // Entre 18 y 23
      const between18and23: number = currentData.subtract(5, "years").year()
      // Entre 24 y 35
      const between24and35: number = currentData.subtract(12, "years").year()
      // Entre 36 y 45
      const between36and45: number = currentData.subtract(10, "years").year()
      // Mayor de 45
      const greaterThan45: number = currentData.subtract(1, "year").year()
      
      return await ReviewModel.aggregate(
        [
          {
            '$project': {
              '_id': 1, 
              'place': {
                '$toObjectId': '$place'
              }, 
              'rating': 1, 
              'user': {
                '$toObjectId': '$user'
              }, 
              'date': 1
            }
          }, {
            '$lookup': {
              'from': 'places', 
              'localField': 'place', 
              'foreignField': '_id', 
              'as': 'place'
            }
          }, {
            '$lookup': {
              'from': 'users', 
              'localField': 'user', 
              'foreignField': '_id', 
              'as': 'user'
            }
          }, {
            '$unwind': {
              'path': '$place'
            }
          }, {
            '$unwind': {
              'path': '$user'
            }
          }, {
            '$project': {
              '_id': 1, 
              'rating': 1, 
              'date': 1, 
              'place': 1, 
              'user': 1, 
              'birthDate': {
                '$dateFromString': {
                  'dateString': '$user.birthDate'
                }
              }, 
              'kindOfPlace': {
                '$toObjectId': '$place.kindOfPlace'
              }
            }
          }, {
            '$lookup': {
              'from': 'kindofplaces', 
              'localField': 'kindOfPlace', 
              'foreignField': '_id', 
              'as': 'kindOfPlace'
            }
          }, {
            '$unwind': {
              'path': '$kindOfPlace'
            }
          }, {
            '$project': {
              'rating': 1, 
              'date': 1, 
              'place': 1, 
              'user': 1, 
              'kindOfPlace': '$kindOfPlace.kind', 
              'yearBorn': {
                '$year': '$birthDate'
              }
            }
          }, {
            '$project': {
              '_id': 1, 
              'rating': 1, 
              'place': 1, 
              'user': 1, 
              'kindOfPlace': 1, 
              'date': 1, 
              'ageRange': {
                '$switch': {
                  'branches': [
                    {
                      'case': {
                        '$gt': [
                          '$yearBorn', lessThan18
                        ]
                      }, 
                      'then': 'Menor de 18'
                    }, {
                      'case': {
                        '$gte': [
                          '$yearBorn', between18and23
                        ]
                      }, 
                      'then': 'Entre 18 y 23'
                    }, {
                      'case': {
                        '$gte': [
                          '$yearBorn', between24and35
                        ]
                      }, 
                      'then': 'Entre 24 y 35'
                    }, {
                      'case': {
                        '$gte': [
                          '$yearBorn', between36and45
                        ]
                      }, 
                      'then': 'Entre 36 y 45'
                    }, {
                      'case': {
                        '$lte': [
                          '$yearBorn', greaterThan45
                        ]
                      }, 
                      'then': 'Mayor de 45'
                    }
                  ]
                }
              }
            }
          }, {
            '$group': {
              '_id': {
                'state': '$user.state', 
                'kind': '$kindOfPlace', 
                'ageRange': '$ageRange'
              }, 
              'users': {
                '$addToSet': '$user'
              }, 
              'ageRange': {
                '$addToSet': '$ageRange'
              }
            }
          }, {
            '$unwind': {
              'path': '$ageRange'
            }
          }, {
            '$project': {
              '_id': 0, 
              'state': '$_id.state', 
              'kindOfPlace': '$_id.kind', 
              'ageRange': 1, 
              'users': {
                '$size': '$users'
              }
            }
          }, {
            '$group': {
              '_id': '$state', 
              'data': {
                '$addToSet': {
                  '$switch': {
                    'branches': [
                      {
                        'case': {
                          '$eq': [
                            '$kindOfPlace', 'Museum'
                          ]
                        }, 
                        'then': {
                          'kind': 'Museos',
                          'ageRange': '$ageRange', 
                          'users': '$users'
                        }
                      }, {
                        'case': {
                          '$eq': [
                            '$kindOfPlace', 'Coffee'
                          ]
                        }, 
                        'then': {
                          'kind': 'Cafeterias', 
                          'ageRange': '$ageRange', 
                          'users': '$users'
                        }
                      }, {
                        'case': {
                          '$eq': [
                            '$kindOfPlace', 'Park'
                          ]
                        }, 
                        'then': {
                          'kind': 'Parques', 
                          'ageRange': '$ageRange', 
                          'users': '$users'
                        }
                      }, {
                        'case': {
                          '$eq': [
                            '$kindOfPlace', 'Bar'
                          ]
                        }, 
                        'then': {
                          'kind': 'Bares', 
                          'ageRange': '$ageRange', 
                          'users': '$users'
                        }
                      }, {
                        'case': {
                          '$eq': [
                            '$kindOfPlace', 'Food'
                          ]
                        }, 
                        'then': {
                          'kind': 'Comida', 
                          'ageRange': '$ageRange', 
                          'users': '$users'
                        }
                      }
                    ]
                  }
                }
              }
            }
          }, {
            '$project': {
              '_id': 0, 
              'state': '$_id', 
              'data': 1
            }
          }
        ]
      )
    } catch (error) {
      console.log(error)
      throw new ApolloError("Ocurrio un error")
    }
  }
}