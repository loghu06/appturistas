import ReviewModel from '../models/Review'
import { Movement } from '../types/Movement.type';
import { Review } from '../types/Review.type';
import { ApolloError } from 'apollo-server-koa';
import moment from '../helpers/moment.helper'
import { Service } from 'typedi';

@Service()
export class ReviewService {    
    // Get all reviews of a place
    public async getReviews(placeID: string){
        try {
            return await ReviewModel.find({place: placeID})
            .populate({
                path: "user",
                model: "User"
            })
        } catch (error) {
            console.log(error)
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Get all reviews of a user
    public async getUserReviews(user: string){
        try {
            return await ReviewModel.find({user: user})
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Get a user review 
    public async getReview(placeID: string, userID: string){
        try {
            return await ReviewModel.findOne({
                place: placeID,
                user: userID
            })
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Create a review
    public async createReview(reviewBody: Review): Promise<Movement>{
        try {
            reviewBody.date = moment(new Date()).format("MMMM DD, YYYY HH:mm")
            let review = await new ReviewModel(reviewBody).save()
            
            return {
                message: "Comentario fue creado con exito",
                isSuccess: true
            }
        } catch (error) {
            console.log(error)
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Update a review
    public async updateReview(reviewBody: Review, userID: string): Promise<Movement>{
        try {
            let review = await ReviewModel.findOne({_id: reviewBody._id, user: userID})

            if(review){
                await review.updateOne(reviewBody)
                return {
                    message: "Comentario actualizado", isSuccess: true
                }
            }
            else {
                return {
                    message: "No puedes editar este comentario", isSuccess: false
                }
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Delete a review (By review id and user id to ensure the review belongs 
    // to the user who wants to delete it)
    public async deleteReview(reviewID: string, userID: string): Promise<Movement>{
        try {
            let review = await ReviewModel.findOne({_id: reviewID, user: userID})

            if(review){
                await review.remove()
                return {
                    message: "Comentario eliminado", isSuccess: true
                }
            }
            else {
                return {
                    message: "No puedes eliminar este comentario", isSuccess: false
                }
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
}