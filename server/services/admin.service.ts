import AdminModel from '../models/Admin'
import UserModel from '../models/User'
import { Movement } from '../types/Movement.type'
import { generatePicture } from '../helpers/picture.helper';
import { ApolloError } from 'apollo-server-koa';
import { mailer } from '../helpers/email.helper';
import { Admin } from '../types/Admin.type';
import { MongoError } from 'mongodb'
import { Service } from 'typedi';

@Service()
export class AdminService {
    // Create an admin
    public async createAdmin(adminBody: Admin): Promise<Movement>{
        try {
            if(await UserModel.findOne({email: adminBody.email})){
                let error: MongoError = new MongoError("Email ya se encuentra en uso")
                error.code = 11000
                throw error
            }
            let admin = new AdminModel(adminBody)
            admin.picture = generatePicture(`${admin.name} ${admin.lastName}`)
            await admin.save()
            
            return {
                message: "Administrador creado",
                isSuccess: true
            }
        } catch (error) {
            console.log(error)
            if(error.code == 11000){
                return {
                    message: "Email ya se encuentra en uso",
                    isSuccess: false
                }
            }
            else {
                throw new ApolloError("Ocurrio un error")
            }
        }
    }
    // Login
    public async adminLogin(email: string, password: string): Promise<Movement>{
        try {
            let admin = await AdminModel.findOne({email})

            if(admin){
                if(admin.comparePassword(password)){
                    return {
                        isSuccess: true,
                        message: admin.generateToken()
                    }
                }
                else {
                    return {
                        isSuccess: false,
                        message: "Contraseña invalida"
                    }
                }
            }
            else {
                return {
                    isSuccess: false,
                    message: "No se encontro un usuario con este correo"
                }
            }
        } catch (error) {
            return {
                isSuccess: false,
                message: "Ocurrio un error"
            }
        }
    }
    // Update an admin
    public async updateAdmin(adminBody: Admin, id: string): Promise<Movement>{
        try {
            await AdminModel.findByIdAndUpdate(id, adminBody)
            
            return {
                message: "Usuario actualizado",
                isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Get an admin by id (Id is provided by a token)
    public async findAdminById(id: string): Promise<Admin> {
        try {
            return await AdminModel.findById(id)
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Delete an admin by id (Id is provided by a token)
    public async deleteAdmin(id: string): Promise<Movement>{
        try {
            await AdminModel.findByIdAndRemove(id)
            
            return {
                message: "Cuenta de usuario eliminada", isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
}