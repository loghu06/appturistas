import { Service } from "typedi";
import { KindOfPlace } from "../types/KindOfPlace.type";
import KindModel from '../models/KindOfPlace'
import { ApolloError } from "apollo-server-koa";
import { Movement } from "../types/Movement.type";

@Service()
export class kindOfPlaceService {
    // Get all kind of places
    public async getKindOfPlaces(): Promise<Array<KindOfPlace>>{
        try {
            return await KindModel.find()
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Create a kind of place
    public async createKind(kind: KindOfPlace): Promise<Movement>{
        try {
            await new KindModel(kind).save()

            return {
                message: "Tipo de lugar creado",
                isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
    // Update a kind of place
    public async updateKind(kind: KindOfPlace): Promise<Movement>{
        try {
            await KindModel.findByIdAndUpdate(kind._id, kind)

            return {
                message: "Tipo de lugar actualizado",
                isSuccess: true
            }
        } catch (error) {
            throw new ApolloError("Ocurrio un error")
        }
    }
}