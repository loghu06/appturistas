import { ConnectionOptions } from 'mongoose'
import { readFileSync } from 'fs';

export const MONGO_URI: string = "mongodb://localhost:27017/admin"
// export const MONGO_URI: string = "mongodb+srv://devcluster-ue8gw.mongodb.net/admin"
/* export const MONGO_CONFIG: ConnectionOptions = {
    user: "testing",
    pass: "NCVrokYrQwhXBxuh",
    dbName: "tourists",
    useNewUrlParser: true,
    useFindAndModify: true,
    useCreateIndex: true
} */
export const MONGO_CONFIG: ConnectionOptions = {
    user: "root",
    pass: "masterkey123",
    dbName: "tourists",
    useNewUrlParser: true,
    useFindAndModify: true,
    useCreateIndex: true
}
export const ROUNDS: number = 10
export const PORT: number = parseInt(process.env.PORT) || 7000
// A file with the key can be used
export const SECRET_KEY: string = readFileSync('keyfile', 'utf-8')