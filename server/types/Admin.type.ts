import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { IsEmail, MinLength } from 'class-validator'

@ObjectType()
export class Admin {
    @Field(type => ID)
    _id: string // Mongo id
    @Field()
    name: string // User's name
    @Field()
    lastName: string // User's lastname
    @Field()
    birthDate: string
    @Field()
    state: string
    @Field()
    email: string // User's email
    @Field()
    password: string // User's password
    @Field({nullable: true})
    picture?: string // User's profile picture
}

@InputType()
export class AdminInput implements Partial<Admin> {
    @Field(type => ID, {nullable: true})
    _id: string // Mongo id
    @Field({ nullable: true })
    name: string // User's name
    @Field({ nullable: true })
    lastName: string // User's lastname
    @Field({ nullable: true })
    birthDate: string
    @Field({ nullable: true })
    state: string
    @Field({ nullable: true })
    @IsEmail()
    email: string // User's email
    @Field({ nullable: true })
    @MinLength(6)
    password: string
    @Field({nullable: true})
    picture?: string // User's profile picture
}