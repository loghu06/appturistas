import { ObjectType, Field } from 'type-graphql'

@ObjectType()
export class Movement {
    @Field()
    message: string
    @Field()
    isSuccess: boolean
    @Field({nullable: true})
    token?: string
}