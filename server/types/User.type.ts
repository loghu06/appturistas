import { ObjectType, Field, ID, InputType } from 'type-graphql'
import { IsEmail, MinLength } from 'class-validator'
import { KindOfPlace } from './KindOfPlace.type'
import fieldConfig from 'graphql-query-complexity/dist/estimators/fieldConfig'

@ObjectType()
export class User {
    @Field(type => ID)
    _id: string // Mongo id
    @Field()
    name: string // User's name
    @Field()
    lastName: string // User's lastname
    @Field()
    birthDate: string
    @Field()
    state: string
    @Field()
    email: string // User's email
    @Field()
    password: string // User's password
    @Field({nullable: true})
    picture?: string // User's profile picture
    @Field(type => [KindOfPlace], {nullable: true})
    favoritePlaces: Array<KindOfPlace>
}

@InputType()
export class UserInput implements Partial<User> {
    @Field(type => ID, {nullable: true})
    _id: string // Mongo id
    @Field({ nullable: true })
    name: string // User's name
    @Field({ nullable: true })
    lastName: string // User's lastname
    @Field({ nullable: true })
    birthDate: string
    @Field({ nullable: true })
    state: string
    @Field({ nullable: true })
    @IsEmail()
    email: string // User's email
    @Field({ nullable: true })
    @MinLength(6)
    password: string
    @Field({nullable: true})
    picture?: string // User's profile picture
    @Field(type => [ID], {nullable: true})
    favoritePlaces: Array<KindOfPlace>
}