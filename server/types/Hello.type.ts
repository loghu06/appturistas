import { ObjectType, Field, InputType } from 'type-graphql'
import { Contains, IsEmail } from 'class-validator';

@ObjectType()
export class Hello {
    @Field()
    message: string
    @Field()
    isWorking: boolean
}

@InputType()
export class HelloInput implements Partial<Hello> {
    @Field()
    @IsEmail()
    email: string
    @Field()
    message: string
    @Field()
    isWorking: boolean
}