import { Field, ObjectType, Int } from "type-graphql";

@ObjectType()
export class Stats {
  @Field()
  state: string
  @Field(type => [PlaceData])
  data: Array<PlaceData>
}

@ObjectType()
class PlaceData {
  @Field()
  kind: string
  @Field()
  ageRange: string
  @Field(type => Int)
  users: number
}

/* 

[
  {
    '$project': {
      '_id': 1, 
      'place': {
        '$toObjectId': '$place'
      }, 
      'rating': 1, 
      'user': {
        '$toObjectId': '$user'
      }, 
      'date': 1
    }
  }, {
    '$lookup': {
      'from': 'places', 
      'localField': 'place', 
      'foreignField': '_id', 
      'as': 'place'
    }
  }, {
    '$lookup': {
      'from': 'users', 
      'localField': 'user', 
      'foreignField': '_id', 
      'as': 'user'
    }
  }, {
    '$unwind': {
      'path': '$place'
    }
  }, {
    '$unwind': {
      'path': '$user'
    }
  }, {
    '$project': {
      '_id': 1, 
      'rating': 1, 
      'date': 1, 
      'place': 1, 
      'user': 1, 
      'birthDate': {
        '$dateFromString': {
          'dateString': '$user.birthDate'
        }
      }, 
      'kindOfPlace': {
        '$toObjectId': '$place.kindOfPlace'
      }
    }
  }, {
    '$lookup': {
      'from': 'kindofplaces', 
      'localField': 'kindOfPlace', 
      'foreignField': '_id', 
      'as': 'kindOfPlace'
    }
  }, {
    '$unwind': {
      'path': '$kindOfPlace'
    }
  }, {
    '$project': {
      'rating': 1, 
      'date': 1, 
      'place': 1, 
      'user': 1, 
      'kindOfPlace': '$kindOfPlace.kind', 
      'yearBorn': {
        '$year': '$birthDate'
      }
    }
  }, {
    '$project': {
      '_id': 1, 
      'rating': 1, 
      'place': 1, 
      'user': 1, 
      'kindOfPlace': 1, 
      'date': 1, 
      'ageRange': {
        '$switch': {
          'branches': [
            {
              'case': {
                '$gt': [
                  '$yearBorn', 2001
                ]
              }, 
              'then': 'Menor de 18'
            }, {
              'case': {
                '$gte': [
                  '$yearBorn', 1996
                ]
              }, 
              'then': 'Entre 18 y 23'
            }, {
              'case': {
                '$gte': [
                  '$yearBorn', 1984
                ]
              }, 
              'then': 'Entre 24 y 35'
            }, {
              'case': {
                '$gte': [
                  '$yearBorn', 1974
                ]
              }, 
              'then': 'Entre 36 y 45'
            }, {
              'case': {
                '$lte': [
                  '$yearBorn', 1975
                ]
              }, 
              'then': 'Mayor de 45'
            }
          ]
        }
      }
    }
  }, {
    '$group': {
      '_id': {
        'state': '$user.state', 
        'kind': '$kindOfPlace', 
        'ageRange': '$ageRange'
      }, 
      'users': {
        '$addToSet': '$user'
      }, 
      'ageRange': {
        '$addToSet': '$ageRange'
      }
    }
  }, {
    '$unwind': {
      'path': '$ageRange'
    }
  }, {
    '$project': {
      '_id': 0, 
      'state': '$_id.state', 
      'kindOfPlace': '$_id.kind', 
      'ageRange': 1, 
      'users': {
        '$size': '$users'
      }
    }
  }, {
    '$group': {
      '_id': '$state', 
      'data': {
        '$addToSet': {
          '$switch': {
            'branches': [
              {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Museum'
                  ]
                }, 
                'then': {
                  'kind': 'Museum', 
                  'ageRange': '$ageRange', 
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Coffee'
                  ]
                }, 
                'then': {
                  'kind': 'Coffee', 
                  'ageRange': '$ageRange', 
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Park'
                  ]
                }, 
                'then': {
                  'kind': 'Park', 
                  'ageRange': '$ageRange', 
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Bar'
                  ]
                }, 
                'then': {
                  'kind': 'Bar', 
                  'ageRange': '$ageRange', 
                  'users': '$users'
                }
              }, {
                'case': {
                  '$eq': [
                    '$kindOfPlace', 'Food'
                  ]
                }, 
                'then': {
                  'kind': 'Food', 
                  'ageRange': '$ageRange', 
                  'users': '$users'
                }
              }
            ]
          }
        }
      }
    }
  }, {
    '$project': {
      '_id': 0, 
      'state': '$_id', 
      'data': 1
    }
  }
]

*/