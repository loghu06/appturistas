import { ObjectType, Field, ID, Float, InputType } from 'type-graphql'
import { MinLength } from 'class-validator';
import { KindOfPlace } from './KindOfPlace.type';

@ObjectType()
export class Place {
    @Field(type => ID)
    _id: string // Mongo id
    @Field()
    name: string // Place's name
    @Field(type => Float)
    longitude: number // Position in the earth
    @Field(type => Float)
    latitude: number
    @Field(type => KindOfPlace)
    kindOfPlace: KindOfPlace
    @Field(type => [String], {nullable: "itemsAndList"})
    pictures: string[] // Place's pictures
}

@InputType()
export class PlaceInput implements Partial<Place> {
    @Field(type => ID, {nullable: true})
    _id: string // Mongo id
    @Field({nullable: true})
    @MinLength(1)
    name: string // Place's name
    @Field(type => Float, {nullable: true})
    longitude: number // Position in the earth
    @Field(type => Float, {nullable: true})
    latitude: number
    @Field(type => ID, {nullable: true})
    kindOfPlace: KindOfPlace
    @Field(type => [String], {nullable: "itemsAndList"})
    pictures: string[] // Place's pictures
}