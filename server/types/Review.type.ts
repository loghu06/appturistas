import { ObjectType, Field, ID, Int, InputType } from 'type-graphql'
import { Place } from './Place.type'
import { User } from './User.type'

@ObjectType()
export class Review {
    @Field(type => ID)
    _id: string // Mongo id
    @Field(type => User)
    user: User // User who's rating
    @Field()
    message: string
    @Field(type => Place)
    place: Place // Place which is being rated
    @Field()
    date: string // Review's date
    @Field(type => Int)
    rating: number // How good the place is
}

@InputType()
export class ReviewInput implements Partial<Review> {
    @Field(type => ID, {nullable: true})
    _id: string // Mongo id
    @Field(type => ID, {nullable: true})
    user: User
    @Field()
    message: string
    @Field(type => ID)
    place: Place // Place which is being rated
    @Field({nullable: true})
    date: string // Review's date
    @Field(type => Int)
    rating: number // How good the place is
}