import { ObjectType, Field, ID, InputType } from 'type-graphql'

@ObjectType()
export class KindOfPlace {
    @Field(type => ID, { nullable: true })
    _id: string // Mongo id
    @Field()
    kind: string
}

@InputType()
export class KindOfPlaceInput implements Partial<KindOfPlace>{
    @Field(type => ID, {nullable: true})
    _id: string
    @Field()
    kind: string
}