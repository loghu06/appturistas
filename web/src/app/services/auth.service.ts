import { Injectable } from "@angular/core";
import { Apollo } from 'apollo-angular';
import { QueryOptions } from 'apollo-client';
import gql from 'graphql-tag'
import { FetchResult } from 'apollo-link';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthService {

  constructor(
    private _apollo: Apollo,
    private _router: Router,
    private _snackbar: MatSnackBar
  ){}
  
  public async adminLogin(email: string, password: string){
    const adminLoginOptions: QueryOptions = {
      query: gql`
        query AdminLogin($email: String!, $password: String!){
          AdminLogin(email: $email, password: $password){
            message isSuccess
          }
        }
      `,
      variables: {
        email, password
      },
      fetchPolicy: "network-only"
    }

    const response: FetchResult = await this._apollo.query(adminLoginOptions).toPromise()

    if(response.data.AdminLogin.isSuccess){
      localStorage.setItem("authorization", response.data.AdminLogin.message)
      this._router.navigateByUrl("/dashboard", { replaceUrl: true })
    }
    else {
      this._snackbar.open(response.data.AdminLogin.message, "CERRAR",{
        duration: 5000
      })
    }
  }

  public async signOut(){
    localStorage.removeItem("authorization")
    this._router.navigateByUrl("/auth/login", { replaceUrl: true })
  }
}