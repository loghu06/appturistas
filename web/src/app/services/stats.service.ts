import { Injectable } from "@angular/core";
import { Apollo } from 'apollo-angular';
import { QueryOptions } from 'apollo-client';
import gql from 'graphql-tag';
import { Store } from '@ngxs/store';
import { UpdateStats } from '../store/stats.actions';

@Injectable()
export class StatsService {

  constructor(
    private _apollo: Apollo,
    private _store: Store
  ){}
  
  public async getStats(){
    const statsOptions: QueryOptions = {
      query: gql`
        query {
          Stats {
            state data { users ageRange kind }
          }
        }
      `
    }

    const response: any = await this._apollo.query(statsOptions).toPromise()
    this._store.dispatch(new UpdateStats(response.data.Stats))
  }
}