import { Injectable } from "@angular/core";
import { StatsBody } from '../store/stats.actions';

@Injectable()
export class ChartService {

  public orderByKindOfPlace(stats: Array<StatsBody>){
    let _orderedStats: any = {}
    let _orderedArray: Array<any> = []

    stats.forEach(_stat => {
      // State won't be twice in a response
      if(_orderedStats[_stat.state] == null){
        _orderedStats[_stat.state] = {
          state: _stat.state,
          data: {}
        }

        _stat.data.forEach(_data => {
          if(_orderedStats[_stat.state]["data"][_data.kind] == null){
            _orderedStats[_stat.state]["data"][_data.kind] = []
          }

          _orderedStats[_stat.state]["data"][_data.kind].push(_data.users)
        })
      }
    })

    for(let key in _orderedStats){
      for(let kind in _orderedStats[key].data){
        let _counter: number = 0

        for(let i = 0 ; i < _orderedStats[key].data[kind].length ; i++){
          _counter += _orderedStats[key].data[kind][i]
        }

        _orderedStats[key].data[kind] = _counter
        // console.log("Result: ", _counter)
      }

      _orderedArray.push(_orderedStats[key])
    }

    // console.log(stats)
    // console.log(_orderedArray)
    return _orderedArray
  }

  public orderByState(stats: any){

  }
}