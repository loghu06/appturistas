import { NgModule } from "@angular/core";

// Apollo tools
import { HttpLink, HttpLinkModule, HttpLinkHandler } from 'apollo-angular-link-http'
import { Apollo, ApolloModule } from 'apollo-angular'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink, split } from 'apollo-link'
import { HttpClientModule, HttpHeaders } from '@angular/common/http';

@NgModule({
  exports: [
    HttpLinkModule,
    HttpClientModule,
    ApolloModule
  ]
})
export class GraphQLModule {

  constructor(
    private _apollo: Apollo,
    private _httpLink: HttpLink
  ){
    const uri: string = "https://tourists-app.herokuapp.com/graphql"
    const _http: HttpLinkHandler = this._httpLink.create({uri})

    const _authorizationHeader: ApolloLink = new ApolloLink((operation, forward) => {
      operation.setContext({
        headers: new HttpHeaders().set("authorization", localStorage.getItem("authorization") || "")
      })

      return forward(operation)
    })

    const link = ApolloLink.from([_authorizationHeader, _http])

    this._apollo.create({
      link,
      cache: new InMemoryCache()
    })
  }
}