import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: "",
    loadChildren: () => import("./pages/authentication/authentication.module").then(m => m.AuthenticationModule)
  },
  {
    path: "",
    loadChildren: () => import("./pages/dashboard/dashboard.module").then(m => m.DashboardModule)
  },
  {
    path: "**",
    redirectTo: "/dashboard"
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {}