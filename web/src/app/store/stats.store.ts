import { State, Action, StateContext } from '@ngxs/store'
import { StatsBody, UpdateStats } from './stats.actions'

interface StatsModel {
  stats: Array<StatsBody>
}

@State<StatsModel>({
  name: "Stats",
  defaults: {
    stats: []
  }
})
export class StatsState {
  @Action(UpdateStats)
  updateStats(
    { patchState }: StateContext<UpdateStats>,
    { stats }: UpdateStats
  ){
    patchState({stats})
  }
}