interface DataBody {
  kind: string
  ageRange: string
  users: number
}

export interface StatsBody {
  state: string
  data: Array<DataBody>
}

export class UpdateStats {
  public static readonly type: string = "[Stats] UpdateStats"

  constructor(public stats: Array<StatsBody>){}
}