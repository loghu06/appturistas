import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import { AuthenticationRoutingModule } from './authentication.routing';
import { LoginPage } from './login/login.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  MatFormFieldModule,
  MatButtonModule,
  MatInputModule,
  MatSnackBarModule
} from '@angular/material'

@NgModule({
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // Material modules
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule
  ],
  declarations: [
    AuthenticationComponent,
    LoginPage
  ]
})
export class AuthenticationModule {

}