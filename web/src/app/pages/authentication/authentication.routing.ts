import { Routes, RouterModule } from '@angular/router'
import { NgModule } from '@angular/core'
import { AuthenticationComponent } from './authentication.component'
import { LoginPage } from './login/login.page'
import { AuthenticationGuard } from './authentication.guard'

const routes: Routes = [
  {
    path: "auth",
    component: AuthenticationComponent,
    // canActivateChild: [AuthenticationGuard],
    children: [
      {
        path: "login",
        component: LoginPage
      },
      /* {
        path: "**",
        redirectTo: "login"
      } */
    ]
  },
  /* {
    path: "**",
    redirectTo: "/login"
  } */
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule {}