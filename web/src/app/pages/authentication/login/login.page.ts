import { Component } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: "login-page",
  templateUrl: "login.page.html",
  styleUrls: ["login.page.scss"],
  providers: [AuthService]
})
export class LoginPage {
  public loginForm: FormGroup

  constructor(
    private _formBuilder: FormBuilder,
    private _authService: AuthService
  ){
    this.loginForm = this._formBuilder.group({
      email: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.minLength(6)])
    })
  }

  public adminLogin(){
    if(this.loginForm.valid){
      const { email, password } = this.loginForm.value

      this._authService.adminLogin(email, password)
    }
  }
}