import { Component, OnInit, OnDestroy } from "@angular/core";
import { StatesList } from '../../../helpers/states.helper'
import { StatsService } from 'src/app/services/stats.service';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { StatsBody } from 'src/app/store/stats.actions';
import { ChartService } from 'src/app/services/chart.service';

@Component({
  selector: "home-page",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
  providers: [StatsService, ChartService]
})
export class HomePage implements OnInit, OnDestroy {
  public statesList = StatesList
  public searchByState: FormControl
  public orderedList: Array<any> = []
  private _allStats: any = []
  private _stats$: Subscription
  private _search$: Subscription

  constructor(
    private _statsService: StatsService,
    private _chartService: ChartService,
    private _store: Store
  ){
    this._statsService.getStats()
  }
  
  ngOnInit(){
    this.searchByState = new FormControl("all")
    
    this._stats$ = this._store.select(({ Stats }) => Stats.stats)
    .subscribe( (stats: Array<StatsBody>) => {
      this._allStats = stats

      if(stats.length > 0){
        this.orderedList = this._chartService.orderByKindOfPlace(stats)
      }
      // if(this.orderedList.length > 0){
      //   console.log(this.orderedList[0].data)
      // }
    })
    
    this._search$ = this.searchByState
    .valueChanges
    .subscribe((search: string) => {
      if(search != "all"){
        const _stateStats = this._allStats.filter(_state => _state.state == search)
        console.log(_stateStats[0])
      }
    })
  }

  ngOnDestroy(){
    this._stats$.unsubscribe()
    this._search$.unsubscribe()
  }
}