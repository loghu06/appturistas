import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardRootPage } from './dashboard.component';
import { HomePage } from './home/home.page';
import { RouterModule } from '@angular/router';

// Material modules
import { 
  MatSidenavModule, 
  MatToolbarModule, 
  MatButtonModule, 
  MatIconModule, 
  MatSelectModule, 
  MatCardModule
} from '@angular/material'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GeneralChartComponent } from 'src/app/components/general-chart/general-chart.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatCardModule
  ],
  declarations: [
    DashboardRootPage,
    HomePage,
    GeneralChartComponent
  ]
})
export class DashboardModule {}