import { Component } from "@angular/core";
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: "dashboard-component",
  templateUrl: "dashboard.component.html",
  styleUrls: ["dashboard.component.scss"],
  providers: [AuthService]
})
export class DashboardRootPage {

  constructor(
    private _authService: AuthService
  ){
    console.log("Dashboard component")
  }
  
  public signOut(){
    this._authService.signOut()
  }
}