import { Injectable } from "@angular/core";
import { CanActivateChild, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt'

@Injectable({providedIn: "root"})
export class DashboardGuard implements CanActivateChild {
  private jwt: JwtHelperService = new JwtHelperService()

  constructor(
    private _router: Router
  ){}
  
  canActivateChild(){
    let canNavigate: boolean = true
    const token: string = localStorage.getItem("authorization")

    if(this.jwt.isTokenExpired(token)){
      canNavigate = false
      this._router.navigateByUrl("/auth/login", { replaceUrl: true })
    }

    
    return canNavigate
  }

}