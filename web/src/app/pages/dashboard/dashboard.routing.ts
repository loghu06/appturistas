import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { DashboardRootPage } from './dashboard.component';
import { HomePage } from './home/home.page';
import { DashboardGuard } from './dashboard.guard';

const routes: Routes = [
  {
    path: "dashboard",
    component: DashboardRootPage,
    canActivateChild: [DashboardGuard],
    children: [
      {
        path: "",
        component: HomePage
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}