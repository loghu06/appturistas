import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app.routing';

import { AuthenticationModule } from './pages/authentication/authentication.module';
import { GraphQLModule } from './graphql/GraphQL.module'; // GraphQL module
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { NgxsModule } from '@ngxs/store';
import { StatsState } from './store/stats.store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    GraphQLModule,
    AuthenticationModule,
    DashboardModule,
    NgxsModule.forRoot([
      StatsState
    ], 
    {
      developmentMode: true
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
