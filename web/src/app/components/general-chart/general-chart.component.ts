import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import Chart from 'chart.js'
import randomHex from 'random-hex'

@Component({
  selector: "general-chart",
  templateUrl: "general-chart.component.html",
  styleUrls: ["general-chart.component.scss"]
})
export class GeneralChartComponent implements OnInit, OnDestroy {
  @Input() data: any
  @Input() state: string
  private chart: any
  private _labels: Array<string> = []
  private _datasets = [{
    data: [],
    backgroundColor: []
  }]

  private _makeChart(): void {
    // context
    // const context = document.getElementById(this.state)
    let context = document.getElementById(this.state)
    
    const chart = new Chart(context, {
      type: "pie",
      data: {
        datasets: this._datasets,
        labels: this._labels
      }
    })
  }
  
  ngOnInit(){
    console.log(this.state, this.data)

    for(let key in this.data){
      // console.log(key)
      this._labels.push(key)
      this._datasets[0].data.push(this.data[key])
      this._datasets[0].backgroundColor.push(randomHex.generate())
      // this._colors = randomHex.generate()
    }

    if(this.state != null){
      setTimeout(() => {
        this._makeChart()
      }, 300);
    }
  }

  ngOnDestroy(){}
}